//
//  main.m
//  WMDemo
//
//  Created by DTan on 5/12/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
