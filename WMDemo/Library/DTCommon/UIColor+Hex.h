#import <UIKit/UIKit.h>

@interface UIColor (Hex)
+ (UIColor*) colorWithRGBHex: (UInt32) hex;
+ (UIColor*) colorWithRGBAHex: (UInt32) hex;
+ (UIColor*) colorWithHexString: (NSString*) stringToConvert;
@end

@interface UIColor (Colors)
+ (UIColor*) colorWithName: (NSString*) cssColorName;
@end

@interface UIColor(HexString)
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
+ (UIColor *) colorWithHexString: (NSString *) hexString;
@end
