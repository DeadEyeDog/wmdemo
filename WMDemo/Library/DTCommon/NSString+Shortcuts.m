//
//  NSString+Shortcuts.m
//  BMS
//
//  Created by David Tan on 12/12/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#import "NSString+Shortcuts.h"

@implementation NSString (Shortcuts)
- (BOOL)isBlank {
    BOOL isBlank = NO;
    if([self isEqualToString:@""]) {
        isBlank = YES;
    }
    
    return isBlank;
}

- (NSString *)stringWithWhiteSpaceTrimmed {
    NSString *string = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return string;
}

- (NSString *)getHost {
    if(!self || !self.length) {
        return nil;
    }
    
    NSRange range = [self rangeOfString:@"://"];
    NSString *hostURL;
    
    if(range.location != NSNotFound) {
        hostURL = [self substringFromIndex:range.location+range.length];
    }
    
    range = [hostURL rangeOfString:@"/"];
    while(range.location != NSNotFound) {
        hostURL = [hostURL substringToIndex:range.location];
        range = [hostURL rangeOfString:@"/"];
    }
    
    return hostURL;
}

+ (NSString *)fileNameWithBase:(NSString *)base prefix:(NSString *)prefix suffix:(NSString *)suffix extension:(NSString *)extension {
    NSString * fileName = [NSString stringWithFormat:@"[%@]", base];
    if(prefix) {
        fileName = [NSString stringWithFormat:@"[%@]-%@", prefix, fileName];
    }
    
    if(suffix) {
        fileName = [fileName stringByAppendingFormat:@"-[%@]", suffix];
    }
    
    if(extension) {
        fileName = [fileName stringByAppendingFormat:@".%@", extension];
    }
    
    return fileName;
}
@end
