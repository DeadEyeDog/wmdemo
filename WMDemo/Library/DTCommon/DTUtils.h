//
//  MathUtils.h
//  BMS
//
//  Created by David Tan on 12/13/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef DTUtils_h
#define DTUtils_h

#define CREATE_WEAK_SELF(NAME)  __weak typeof(self) NAME = self;
#define CREATE_WEAK(OBJ, NAME)  __weak typeof(OBJ) NAME = OBJ;
#define CREATE_BLOCK(OBJ, NAME)  __block typeof(OBJ) NAME = OBJ;

// normal casting function
#define SAFE_CAST(OBJECT, TYPE) ({ id obj=OBJECT;\
                                    [obj isKindOfClass:[TYPE class]] ? (TYPE *) obj: nil; \
                                })

// for protocols
#define PSAFE_CAST(OBJECT, PROTOCOL) ({ \
                                    id obj = OBJECT;\
                                    obj = [obj conformsToProtocol:@protocol(PROTOCOL)] ? (id<PROTOCOL>)obj : nil; \
                                })

extern id DTSafeCast(id object, Class aClass);
extern BOOL DTValidateString(NSString *string);
extern BOOL DTSystemVersionEqualTo(NSString *v);
extern BOOL DTSystemVersionGreaterThan(NSString *v);
extern BOOL DTSystemVersionGreaterThanOrEqualTo(NSString *v);
extern BOOL DTSystemVersionLessThan(NSString *versionNumber);
extern BOOL DTSystemVersionLessThanOrEqualTo(NSString *v);

#define IS_WIDESCREEN_IOS7 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_WIDESCREEN_IOS8 ( fabs( ( double )[ [ UIScreen mainScreen ] nativeBounds ].size.height - ( double )1136 ) < DBL_EPSILON )
#define IS_WIDESCREEN      ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_WIDESCREEN_IOS8 : IS_WIDESCREEN_IOS7 )

#endif