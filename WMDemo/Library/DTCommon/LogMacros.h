//
//  LogMacros
//  PMGDalClient
//
//  Created by David Tan on 8/29/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#ifndef LogMacros_Header_h
#define LogMacros_Header_h

#ifdef DEBUG
// VERBOSITY - Larger the number the more wordy it gets.
enum {
    VERBOSITY_SILENT  =   -1,
    VERBOSITY_ERROR   =   1,
    VERBOSITY_WARNING =   5,
    VERBOSITY_TRACE   =   10,
};

#   ifndef VERBOSITY
#       define VERBOSITY VERBOSITY_WARNING
#   endif

#   define CONDITIONLOG(condition, xx, ...) { \
                                                if ((condition)) { \
                                                    NSLog(xx, ##__VA_ARGS__); \
                                                } \
                                            } ((void)0)
#   define TRACE_LOG(xx, ...) { \
                                if (VERBOSITY >= VERBOSITY_TRACE) { \
                                    NSLog(xx, ##__VA_ARGS__); \
                                } \
                              } ((void)0)

#   define WARNING_LOG(xx, ...) { \
                                    if (VERBOSITY >= VERBOSITY_WARNING) { \
                                        NSLog(xx, ##__VA_ARGS__); \
                                    } \
                                } ((void)0)

#   define ERROR_LOG(xx, ...) { \
                                    if (VERBOSITY >= VERBOSITY_ERROR) { \
                                        NSLog(xx, ##__VA_ARGS__); \
                                    } \
                                } ((void)0)

#else
#   define CONDITIONLOG(condition, xx, ...) ((void)0)
#   define TRACE_LOG(xx, ...) ((void)0)
#   define WARNING_LOG(xx, ...) ((void)0)
#   define ERROR_LOG(xx, ...) ((void)0)
#endif // #ifdef DEBUG

#endif //PMGDalClient_Header_h
