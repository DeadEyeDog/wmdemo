//
//  MathUtils.m
//  BMS
//
//  Created by David Tan on 12/13/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#import "DTUtils.h"
#import "NSString+Shortcuts.h"
#import <UIKit/UIKit.h>

id DTSafeCast(id object, Class aClass) {
    id obj = object;
    return [obj isKindOfClass:aClass] ? obj: nil;
}

BOOL DTValidateString(NSString *string) {
    // trim white space
    NSString *checkString = [string stringWithWhiteSpaceTrimmed];
    
    // check for nil and empty
    return ((checkString && ![checkString isEqualToString:@""]) ? YES : NO);
}

//#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
BOOL DTSystemVersionEqualTo(NSString *v) {
    return [[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame;
}

//#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
BOOL DTSystemVersionGreaterThan(NSString *v) {
    return [[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending;
}

//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
BOOL DTSystemVersionGreaterThanOrEqualTo(NSString *v) {
    return [[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending;
}

//#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
BOOL DTSystemVersionLessThan(NSString *versionNumber) {
    return [[[UIDevice currentDevice] systemVersion] compare:versionNumber options:NSNumericSearch] == NSOrderedAscending;
}

//#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
BOOL DTSystemVersionLessThanOrEqualTo(NSString *v) {
    return [[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending;
}
