//
//  NSString+Shortcuts.h
//  BMS
//
//  Created by David Tan on 12/12/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Shortcuts)
// checks if there are no characters at all
@property (nonatomic, readonly) BOOL isBlank;

- (NSString *)stringWithWhiteSpaceTrimmed;

/**
 For a url like http://www.google.com/somepage
 This funciton will return www.google.com stripping away characters from the front and back of the string.
 
 @return host string
 */
- (NSString *)getHost;

/**
 *  Easy way to generate a name
 *  Example
 *  base users
 *  prefix january
 *  suffix med info
 *  extension txt
 *  This would generate [january]-[users]-[med info].txt
 *
 */
+ (NSString *)fileNameWithBase:(NSString *)base prefix:(NSString *)prefix suffix:(NSString *)suffix extension:(NSString *)extension;
@end
