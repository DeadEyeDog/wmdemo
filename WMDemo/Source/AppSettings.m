//
//  AppSettings.m
//  WMDemo
//
//  Created by DTan on 5/13/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "AppSettings.h"
#import "UIColor+Hex.h"

@implementation AppSettings

+ (instancetype)sharedInstance {
    static AppSettings *appSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appSettings = [[self alloc] init];
    });
    return appSettings;
}

- (void)appearanceSettings {
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRGBHex:0xce0e02]];
}
@end
