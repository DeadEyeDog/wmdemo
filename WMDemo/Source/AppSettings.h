//
//  AppSettings.h
//  WMDemo
//
//  Created by DTan on 5/13/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject
+ (instancetype)sharedInstance;
- (void)appearanceSettings;
@end
