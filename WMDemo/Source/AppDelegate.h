//
//  AppDelegate.h
//  WMDemo
//
//  Created by DTan on 5/12/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

