//
//  NotificationView.m
//  WMDemo
//
//  Created by DTan on 5/16/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "NotificationView.h"

@implementation NotificationView
- (void)setText:(NSString *)text showActvityIndicator:(BOOL)show {
    _label.text = text;
    _activityIndicatorView.hidden = !show;
}
@end
