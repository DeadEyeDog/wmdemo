//
//  NotificationView.h
//  WMDemo
//
//  Created by DTan on 5/16/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationView : UIView
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
- (void)setText:(NSString *)text showActvityIndicator:(BOOL)show;
@end
