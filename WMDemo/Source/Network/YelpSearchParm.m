//
//  YelpParameters.m
//  Test
//
//  Created by DTan on 5/14/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "YelpSearchParm.h"
/*
El Farolito",
 La Palma Mexicatessen
 */
@implementation YelpSearchParm
- (instancetype)initWithLocation:(NSString *)location  term:(NSString *)term{
    self = [super init];
    if (self) {
        _location = location;
        _term = term;
        _offset = 0;
    }
    
    return self;
}

+ (instancetype)parmWithLocation:(NSString *)location term:(NSString *)term {
    YelpSearchParm *parm = [[YelpSearchParm alloc] initWithLocation:location term:term];
    return parm;
}

+ (instancetype)parmWithLocation:(NSString *)location term:(NSString *)term page:(NSUInteger)page {
    YelpSearchParm *parm = [[YelpSearchParm alloc] initWithLocation:location term:term];
    parm.offset = page * YelpSearchParm.businessesPerRequest;
    return parm;
}

- (id)copyWithZone:(nullable NSZone *)zone {
    id copy = [[[self class] alloc] init];
    if(copy) {
        [copy setLocation:self.location];
        [copy setTerm:self.term];
        [copy setOffset:self.offset];
    }
    
    return copy;
}

+ (instancetype)parmForNextBatchOfBusinesses:(YelpSearchParm *)parm total:(NSUInteger)totalBusinesses {
    if(parm.offset == totalBusinesses) {
        return nil;
    }
    
    YelpSearchParm *nextParm = [parm copy];
    nextParm.offset += YelpSearchParm.businessesPerRequest;
    
    // limit it
    if(nextParm.offset > totalBusinesses) {
        nextParm.offset = totalBusinesses;
    }
    
    return nextParm;
}

// as of this 5/14/2016 Yelp API 2.0 returns a max of 20 businesses per request 
+ (NSUInteger)businessesPerRequest {
    return 20;
}

- (NSDictionary *)parameters {
    NSAssert(_location, @"This is required for the search to work");
    return @{@"location":_location ?: @"",
             @"term":_term,
             @"offset":[NSString stringWithFormat:@"%ld", (long)_offset]};
}
@end
