//
//  YelpParameters.h
//  Test
//
//  Created by DTan on 5/14/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef enum {
//    BestMatch,
//    Distance,
//    HighestRated,
//} YelpSort;

/**
 Parameters that you toss into a search.
 REFERENCE: https://www.yelp.com/developers/documentation/v2/search_api
 */
@interface YelpSearchParm : NSObject <NSCopying>

//
// REQUIRED
//
@property (nonatomic, strong) NSString *location;

//
// OPTIONAL
//
@property (nonatomic, strong) NSString *term;
@property (nonatomic, assign) NSInteger offset;

//
// CONSTANTS
//
+ (NSUInteger) businessesPerRequest;

// NOT INCLUDED - Not enough time to debug everything
//@property (nonatomic, assign) NSInteger limit;
//@property (nonatomic, assign) YelpSort sort;
//
//// must be under 4000 meters
//@property (nonatomic, assign) NSInteger radiusFilter;

//@property (nonatomic, strong) NSString *categoryFilter;
//@property (nonatomic, assign) BOOL dealsFilter
// parameters using the location by a bounding box
// parameters using the location by a geo location

- (instancetype)initWithLocation:(NSString *)location term:(NSString *)term;;
+ (instancetype)parmWithLocation:(NSString *)location term:(NSString *)term;
+ (instancetype)parmForNextBatchOfBusinesses:(YelpSearchParm *)parm total:(NSUInteger)totalBusinesses;
+ (instancetype)parmWithLocation:(NSString *)location term:(NSString *)term page:(NSUInteger)page;

- (NSDictionary *)parameters;
@end
