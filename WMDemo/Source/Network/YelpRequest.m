//
//  YelpRequest.m
//  Test
//
//  Created by DTan on 5/13/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "YelpRequest.h"
#import "TDOAuth.h"
#import "YelpSearchParm.h"
#import "DTUtils.h"
#import "HTTPStatusCodes.h"
#import "LogMacros.h"

// PRIVATE
@interface YelpRequest()
@end

@implementation YelpRequest
- (instancetype)initWithConsumerKey:(NSString *)consumerKey
                     consumerSecret:(NSString *)consumerSecret
                              token:(NSString *)token
                        tokenSecret:(NSString *)tokenSecret {
    self = [super init];
    if(self) {
        _consumerKey = consumerKey;
        _consumerSecret = consumerSecret;
        _token = token;
        _tokenSecret = tokenSecret;
    }
    
    return self;
}

- (void)GETSearch:(YelpSearchParm *)searchParm success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock {
    NSURLRequest *request = [TDOAuth URLRequestForPath:@"/v2/search"
                                            parameters:[searchParm parameters]
                                                  host:@"api.yelp.com"
                                           consumerKey:_consumerKey
                                        consumerSecret:_consumerSecret
                                           accessToken:_token
                                           tokenSecret:_tokenSecret
                                                scheme:@"https"
                                         requestMethod:@"GET"
                                          dataEncoding:TDOAuthContentTypeUrlEncodedForm
                                          headerValues:nil
                                       signatureMethod:TDOAuthSignatureMethodHmacSha1];
    
    TRACE_LOG(@"URL:%@", request.URL);
    NSURLSession *session = [NSURLSession sessionWithConfiguration:NSURLSessionConfiguration.defaultSessionConfiguration];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *JSON;
        if(data) {
            NSError *JSONError;
            JSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:&JSONError];
            ERROR_LOG(@"Request Error:%@", JSONError);
        }
        
        TRACE_LOG(@"JSON:%@", JSON);
        TRACE_LOG(@"Response:%@", response);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(SAFE_CAST(response, NSHTTPURLResponse).statusCode == kHTTPStatusCodeOK) {
                successBlock(JSON);
            }
            else {
                failureBlock(response, error);
            }
        });
    }];
    [dataTask resume];
}

@end

