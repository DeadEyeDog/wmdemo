//
//  YelpSearchResponse.m
//  WMDemo
//
//  Created by DTan on 5/14/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "YelpSearchResponse.h"
#import "Business.h"
#import "LogMacros.h"

@implementation YelpSearchResponse
- (instancetype)initWithJSON:(NSDictionary *)JSON {
    self = [super init];
    if(self) {
        @try {
            // parse only the information needed
            _totalBusinesses = [JSON[@"total"] unsignedIntegerValue];
            
            // surprisingly not every business has a picture entry 
            NSMutableArray *array = [NSMutableArray array];
            for(NSDictionary *business in JSON[@"businesses"]) {
                NSString *imageURL = business[@"image_url"];
                if(imageURL) {
                    Business *oBusiness = [[Business alloc] init];
                    oBusiness.imageURL = imageURL;
                    [array addObject:oBusiness];
                }
            }
            
            _businesses = array;
        }
        @catch (NSException *exception) {
            ERROR_LOG(@"Ya dun goofed. Problems parsing the JSON");
            ERROR_LOG(@"%@", exception);
        }
    }
    
    return self;
}
@end
