//
//  YelpSearchResponseCollection.h
//  WMDemo
//
//  Created by DTan on 5/15/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YelpSearchResponse;
@class YelpSearchParm;

/**
 A data structure that holds the response data and formats it so that it can be served up to the UI.
 The whole point of this is to keep the view controller thin. Whenever I can I try to move code away 
 from the view controller.
 */
@interface YelpSearchResponseCollection : NSObject

@property (nonatomic, readonly) NSArray *searchResponseDataHistory;

// recalculated after adding search response data
@property (nonatomic, readonly) NSDate *creationDate;
@property (nonatomic, readonly) NSUInteger numberOfBusinesses;
@property (nonatomic, readonly) NSUInteger numberOfPages;
@property (nonatomic, readonly) NSArray *fetchedBusinesses;
@property (nonatomic, readonly) NSArray *indexPathsForLastPage;

// this should never change, all the search responses should have the same term and location
@property (nonatomic, readonly) NSString *term;
@property (nonatomic, readonly) NSString *location;

- (void)addSearchResponse:(YelpSearchResponse *)response parm:(YelpSearchParm *)parm;

@end
