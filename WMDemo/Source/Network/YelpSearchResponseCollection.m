//
//  YelpSearchResponseCollection.m
//  WMDemo
//
//  Created by DTan on 5/15/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

#import "YelpSearchResponseCollection.h"
#import "YelpSearchResponse.h"
#import "YelpSearchParm.h"
#import "NSString+Shortcuts.h"

@interface YelpSearchResponseCollection()
@property (nonatomic, strong) NSMutableArray *mSearchResponseDataHistory;
@property (nonatomic, strong) NSMutableArray *mfetchedBusinesses;
@end

@implementation YelpSearchResponseCollection
@dynamic searchResponseDataHistory;
@dynamic fetchedBusinesses;

- (instancetype)init {
    self = [super init];
    if(self) {
        _creationDate = [NSDate date];
        _mSearchResponseDataHistory = [NSMutableArray array];
        _mfetchedBusinesses = [NSMutableArray array];
    }
    
    return self;
}

- (void)addSearchResponse:(YelpSearchResponse *)response parm:(YelpSearchParm *)parm {
    // calculate
    if(_term.length > 0) {
        NSAssert([_term isEqualToString:[parm.term stringWithWhiteSpaceTrimmed]], @"The term should not change, within a single response collection");
    }
    if(_location.length > 0) {
        NSAssert([_location isEqualToString:[parm.location stringWithWhiteSpaceTrimmed]], @"The location should not change, within a single response collection");
    }
    
    _term = [parm.term stringWithWhiteSpaceTrimmed];
    _location = [parm.location stringWithWhiteSpaceTrimmed];
    _numberOfBusinesses = response.totalBusinesses;
    _numberOfPages = (NSUInteger)ceilf((CGFloat)(response.totalBusinesses)/(CGFloat)(YelpSearchParm.businessesPerRequest));
    
    // add to history
    [_mSearchResponseDataHistory addObject:response];
    
    NSInteger startIndex = _mfetchedBusinesses.count;
    
    // for convenience strip out the businesses and have one long list
    [_mfetchedBusinesses addObjectsFromArray:response.businesses];
    
    NSInteger endIndex = _mfetchedBusinesses.count;
    
    NSMutableArray *indexes = [NSMutableArray array];
    for(NSInteger iter = startIndex; iter < endIndex; iter++) {
        [indexes addObject:[NSIndexPath indexPathForRow:iter inSection:0]];
    }
    _indexPathsForLastPage = [indexes copy];
}

#pragma setter/getter override
- (NSArray *)searchResponseDataHistory {
    return [_mSearchResponseDataHistory copy];
}

- (NSArray *)fetchedBusinesses {
    return [_mfetchedBusinesses copy];
}

@end
