//
//  YelpSearchResponse.h
//  WMDemo
//
//  Created by DTan on 5/14/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

// keeping it slim, if more detailed information was needed to be taken from the JSON, an object mapper should be employed
@interface YelpSearchResponse : NSObject
@property (nonatomic, readonly) NSUInteger totalBusinesses;
@property (nonatomic, readonly) NSArray *businesses;

- (instancetype)initWithJSON:(NSDictionary *)JSON;
@end
