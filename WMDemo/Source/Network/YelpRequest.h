//
//  YelpRequest.h
//  Test
//
//  Created by DTan on 5/13/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YelpSearchParm;

typedef void (^SuccessBlock)(NSDictionary *JSON);
typedef void (^FailureBlock)(NSURLResponse *response, NSError *error);

@interface YelpRequest : NSObject
@property (nonatomic, readonly) NSString *consumerKey;
@property (nonatomic, readonly) NSString *consumerSecret;
@property (nonatomic, readonly) NSString *token;
@property (nonatomic, readonly) NSString *tokenSecret;

- (instancetype)initWithConsumerKey:(NSString *)consumerKey
                     consumerSecret:(NSString *)consumerSecret
                              token:(NSString *)token
                        tokenSecret:(NSString *)tokenSecret;

- (void)GETSearch:(YelpSearchParm *)searchParm success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
