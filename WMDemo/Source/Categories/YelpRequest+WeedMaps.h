//
//  YelpRequest+WeedMaps.h
//  WMDemo
//
//  Created by DTan on 5/14/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "YelpRequest.h"

@interface YelpRequest (WeedMaps)
+ (instancetype)requestForWeedMapsTest;
@end
