//
//  CollectionViewController.m
//  WMDemo
//
//  Created by DTan on 5/15/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <MapKit/MapKit.h>

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "YelpRequest+WeedMaps.h"
#import "YelpSearchParm.h"
#import "YelpSearchResponse.h"
#import "YelpSearchResponseCollection.h"
#import "DTUtils.h"
#import "LogMacros.h"
#import "Business.h"
#import "CWStatusBarNotification.h"
#import "UIImageView+WebCache.h"
#import "NotificationView.h"
#import "YelpSearchHistory.h"

// TODO: Fix auto layout for the text fields or redesign the search fields
// TODO: Use core location to automatically add the city and state

static NSString *const reuseIdentifier = @"Cell";
static NSString *const kUserDefaultsKeyLastLocation = @"kUserDefaultsKeyLastLocation";

@interface CollectionViewController() <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *termTextField;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (assign, nonatomic) NSUInteger currentPage;
@property (strong, nonatomic) YelpSearchResponseCollection *responseCollection;
@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;
@property (assign, nonatomic) BOOL searching;
@property (strong, nonatomic) CWStatusBarNotification *notification;
@property (strong, nonatomic) NotificationView *notificationView;
@property (strong, nonatomic) YelpSearchHistory *searchHistory;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation CollectionViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // INITIALIZE
    _sessionManager = [AFHTTPSessionManager manager];
    _sessionManager.responseSerializer = [AFImageResponseSerializer serializer];
    
    _notification = [CWStatusBarNotification new];
    _notification.notificationAnimationInStyle = CWNotificationAnimationStyleBottom;
    _notification.notificationAnimationOutStyle = CWNotificationAnimationStyleBottom;
    _notification.notificationStyle = CWNotificationStyleStatusBarNotification;
    _notificationView = [[NSBundle mainBundle] loadNibNamed:@"CustomNotificationView" owner:nil options:nil][0];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        if(status == AFNetworkReachabilityStatusNotReachable) {
            [_notificationView setText:@"No Connection" showActvityIndicator:NO];
            [_notification displayNotificationWithView:_notificationView forDuration:2.0];
        }
    }];
    
    _responseCollection = [[YelpSearchResponseCollection alloc] init];
    _searchHistory = [[YelpSearchHistory alloc] init];

    // load up the last location the user entered
    NSString *lastLocation = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsKeyLastLocation];
    _locationTextField.text = lastLocation ?: @"";
    if(lastLocation.length == 0) {
        // if there isn't one go find where the user is
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager requestAlwaysAuthorization];
        [_locationManager startUpdatingLocation];
    }
    
//#define DEBUG_TERM_LOCATION_SET
#ifdef DEBUG_TERM_LOCATION_SET
#   warning DEBUG term and location added
    _termTextField.text = @"Burrito";
    _locationTextField.text = @"Santa Ana, CA";
#endif
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _responseCollection.fetchedBusinesses.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    @try {
        NSString *strURL = [_responseCollection.fetchedBusinesses[indexPath.row] imageURL];
    
        // cancel the previous image request
        [cell.imageView sd_cancelCurrentImageLoad];
        
        // fetch our image
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:strURL]
                          placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    @catch(NSException *exception) {
        // weird array out of bounds occurs when changing orientation on the iPad 
        cell.imageView.image = [UIImage imageNamed:@"placeholder"];
    }
    
    // get the next page if we're at the bottom row
    BOOL isOnLastRow = indexPath.row == _responseCollection.fetchedBusinesses.count - 1;
    if(isOnLastRow && [self hasNextPage]){
        [self searchNextPage];
    }
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 2, 10, 2);
}

#pragma mark - Search
- (IBAction)search {
    // reset
    _currentPage = 0;
    _responseCollection = [[YelpSearchResponseCollection alloc] init];
    [_searchHistory addYelpSearchResponseCollection:_responseCollection];
    
    [self searchForPage:0];
    
    [_termTextField resignFirstResponder];
    [_locationTextField resignFirstResponder];
}

- (void)searchForPage:(NSUInteger)page {
    // handle no connection
    if([AFNetworkReachabilityManager sharedManager].isReachable == NO) {
        // try to pull from the history if it exists
        if([_responseCollection.term.lowercaseString isEqualToString:_termTextField.text.lowercaseString] &&
           [_responseCollection.location.lowercaseString isEqualToString:_locationTextField.text.lowercaseString]) {
            // no point in looking through the history if we have the one we already want
            return;
        }
        
        if(_notification.notificationIsShowing == NO) {
            [_notificationView setText:@"No Connection" showActvityIndicator:NO];
            [_notification displayNotificationWithView:_notificationView forDuration:2.0];
        }
        
        YelpSearchResponseCollection *responseCollection = [_searchHistory searchResponseCollectionForTerm:_termTextField.text location:_locationTextField.text];
        if(responseCollection) {
            _responseCollection = responseCollection;
            [self.collectionView reloadData];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_notificationView setText:@"Using Cached Results" showActvityIndicator:NO];
                [_notification displayNotificationWithView:_notificationView forDuration:2.0];
            });
        }
        return;
    }
    
    //
    // warn the user of conditions that bar them from searching
    //
    
    if(_locationTextField.text.length == 0) {
        [_notificationView setText:@"Location Needed" showActvityIndicator:NO];
        [_notification displayNotificationWithView:_notificationView forDuration:2.0];
        return;
    }
    
    if(_searching) {
        [_notificationView setText:@"Already Searching" showActvityIndicator:NO];
        [_notification displayNotificationWithView:_notificationView forDuration:2.0];
        return;
    }
    
    //
    // we are ok to search
    //
    
    _searching = YES;
    
    if(_notification.notificationIsShowing == NO) {
        [_notificationView setText:@"Downloading" showActvityIndicator:YES];
        [_notification displayNotificationWithView:_notificationView completion:nil];
    }
    
    CREATE_WEAK_SELF(wSelf);
    
    YelpSearchParm *parm = [YelpSearchParm parmWithLocation:_locationTextField.text term:_termTextField.text page:page];
    [[YelpRequest requestForWeedMapsTest] GETSearch:parm success:^(NSDictionary *JSON) {
        wSelf.searching = NO;
        
        // save the resposne
        YelpSearchResponse *response = [[YelpSearchResponse alloc] initWithJSON:JSON];
        [wSelf.responseCollection addSearchResponse:response parm:parm];
        
        // when we start off clean
        if(page == 0) {
            [wSelf.collectionView reloadData];
        }
        else {
            // only add cells when we are appending an existing search
            // no need to update the whole collection view
            [wSelf.collectionView performBatchUpdates:^{
                [self.collectionView insertItemsAtIndexPaths:wSelf.responseCollection.indexPathsForLastPage];
            } completion:^(BOOL finished) {
                
            }];
        }
        
        [wSelf.notification dismissNotification];
    } failure:^(NSURLResponse *response, NSError *error) {
        wSelf.searching = NO;
        
        ERROR_LOG(@"%@", response);
        ERROR_LOG(@"%@", error);

        [wSelf.notification dismissNotification];
        
        // add a delay so that they won't fight
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [wSelf.notificationView setText:@"Error Downloading" showActvityIndicator:NO];
            [wSelf.notification displayNotificationWithView:_notificationView forDuration:2.0];
        });
    }];
}

- (BOOL)hasNextPage {
    return _currentPage != _responseCollection.numberOfPages-1;
}

- (void)searchNextPage {
    _currentPage++;
    [self searchForPage:_currentPage];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _termTextField) {
        [_locationTextField becomeFirstResponder];
    
        // save
        [[NSUserDefaults standardUserDefaults] setObject:_locationTextField.text forKey:kUserDefaultsKeyLastLocation];
    }
    else {
        [self search];
        [_locationTextField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    ERROR_LOG(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    CREATE_WEAK_SELF(wSelf);
    [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        wSelf.locationTextField.text = [NSString stringWithFormat:@"%@, %@", placemarks.firstObject.locality, placemarks.firstObject.administrativeArea];
        TRACE_LOG(@"Location found:%@", wSelf.locationTextField.text);
    }];
}
@end
