//
//  CollectionViewCell.h
//  WMDemo
//
//  Created by DTan on 5/15/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UIImageView *imageView;
@end
