//
//  CollectionViewCell.m
//  WMDemo
//
//  Created by DTan on 5/15/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self.contentView addSubview:_imageView];
}

- (void)layoutSubviews {
    _imageView.frame = self.bounds;
}
@end
