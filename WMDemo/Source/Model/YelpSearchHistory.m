//
//  YelpSearchHistory.m
//  WMDemo
//
//  Created by DTan on 5/16/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import "YelpSearchHistory.h"

@interface YelpSearchHistory()
@property (nonatomic, strong) NSMutableSet *searchResponseCollections;
@end

@implementation YelpSearchHistory
- (instancetype)init {
    self = [super init];
    if(self) {
        _searchResponseCollections = [NSMutableSet set];
        _maxNumberOfCollections = 20;
    }
    
    return self;
}

- (void)addYelpSearchResponseCollection:(YelpSearchResponseCollection *)collection {
    [_searchResponseCollections addObject:collection];

    // remove the oldest
    if(_searchResponseCollections.count > _maxNumberOfCollections) {

        NSMutableArray *mArray = [_searchResponseCollections.allObjects mutableCopy];
        [mArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]]];
        [_searchResponseCollections removeObject:mArray.firstObject];
    }
}

- (YelpSearchResponseCollection *)searchResponseCollectionForTerm:(NSString *)term location:(NSString *)location {
    YelpSearchResponseCollection *collection;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"term like[c] %@ AND location like[c] %@", term, location];
    NSArray *collections = [[_searchResponseCollections allObjects] filteredArrayUsingPredicate:predicate];
    collections = [collections sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"fetchedBusinesses.@count" ascending:NO]]];
    collection = collections.firstObject;
    return collection;
}

@end
