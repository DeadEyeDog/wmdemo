//
//  YelpSearchHistory.h
//  WMDemo
//
//  Created by DTan on 5/16/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YelpSearchResponseCollection;

@interface YelpSearchHistory : NSObject

// once this limit has been reached the oldest entry is let go. The default is 20
@property (nonatomic, assign) NSUInteger maxNumberOfCollections;

- (void)addYelpSearchResponseCollection:(YelpSearchResponseCollection *)collection;
- (YelpSearchResponseCollection *)searchResponseCollectionForTerm:(NSString *)term location:(NSString *)location;
@end
