//
//  WMDemoTests.m
//  WMDemoTests
//
//  Created by DTan on 5/12/16.
//  Copyright © 2016 DDT. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "YelpRequest+WeedMaps.h"
#import "YelpSearchParm.h"
#import "YelpSearchResponse.h"
#import "ZTTestCase.h"
#import "DTUtils.h"
#import "YelpSearchHistory.h"
#import "YelpSearchResponseCollection.h"

@interface YelpSearchHistory()
@property (nonatomic, strong) NSMutableArray *searchResponseCollections;
@end

@interface WMDemoTests : ZTTestCase
@property (nonatomic, assign) NSUInteger pageCount;
@end

@implementation WMDemoTests
- (void)testSimpleYelpSearchRequest {
    [self performAsyncTestWithBlock:^{
        YelpRequest *request = [YelpRequest requestForWeedMapsTest];
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:@"Santa Ana, CA" term:@"Bacon"];
        
        [request GETSearch:parm success:^(NSDictionary *JSON) {
            NSLog(@"Response received");
            XCTAssert(JSON, @"There should be a response object");
            [self asyncTestCompleted];
        } failure:^(id response, NSError *error) {
            NSLog(@"%@", response);
            NSLog(@"%@", error);
            
            XCTFail(@"There was an error.");
        }];
    } timeout:30.0];
}

- (void)GETSearchForRequest:(YelpRequest *)request parm:(YelpSearchParm *)parm success:(SuccessBlock)successBlock{
    CREATE_WEAK_SELF(wSelf);
    [request GETSearch:parm success:^(NSDictionary *JSON) {
        XCTAssert(JSON, @"There should be a response object");
        
        if(successBlock) {
            successBlock(JSON);
        }
        
        YelpSearchResponse *response = [[YelpSearchResponse alloc] initWithJSON:JSON];
        YelpSearchParm *nextParm = [YelpSearchParm parmForNextBatchOfBusinesses:parm total:response.totalBusinesses];
        
        if(parm.offset == 0) {
            NSLog(@"Total %lu",(unsigned long)response.totalBusinesses);
        }
        
        NSLog(@"Name %@",[JSON[@"businesses"] valueForKey:@"name"]);
        NSLog(@"Count:%lu", [JSON[@"businesses"] count]);
        
        if(nextParm) {
            _pageCount++;
            [wSelf GETSearchForRequest:request parm:nextParm success:successBlock];
        }
        else {
            // NOTE: It'd be tempting to check if the count of the arrays are the same as the business total
            // but each business does not guarantee a picture, so that shouldn't be done.
            //XCTAssert(someCount == total, @"Got to the end but didn't pull everything down."); << Don't do that
            
            // perhaps the best we can do is to check to make sure we made the right amount of requests
            CGFloat targetPageCount = ceilf((CGFloat)(response.totalBusinesses)/(CGFloat)(YelpSearchParm.businessesPerRequest));
            XCTAssert(_pageCount == targetPageCount, @"Incorrect page count");
            
            [wSelf asyncTestCompleted];
        }
    } failure:^(id response, NSError *error) {
        NSLog(@"%@", response);
        NSLog(@"%@", error);
        
        XCTFail(@"There was an error.");
    }];
}

// test to see if we can get all businesses for a search location and it's term
- (void)testRequestAllBusinesses {
    _pageCount = 0;
    [self performAsyncTestWithBlock:^{
        YelpRequest *request = [YelpRequest requestForWeedMapsTest];
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:@"Santa Ana, CA" term:@"Satan"];
        
        // recursively pull this down
        [self GETSearchForRequest:request parm:parm success:nil];
    } timeout:500.0];
}

- (void)testRequestForPage {
    NSString *term = @"Satan";
    NSString *location = @"Santa Ana, CA";
    
    CREATE_WEAK_SELF(wSelf);
    [self performAsyncTestWithBlock:^{
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:location term:term];
        [[YelpRequest requestForWeedMapsTest] GETSearch:parm success:^(NSDictionary *JSON) {
            XCTAssert(JSON, @"There should be a response object");
            
            // can't verify this because a business object does not guarantee an image URL
            //YelpSearchResponse *response = [[YelpSearchResponse alloc] initWithJSON:JSON];
            //XCTAssert([response.imageURLs[0] isEqual:_imageURLs[page*YelpSearchParm.businessesPerRequest]], @"These should be the same");
            
            [wSelf asyncTestCompleted];
        } failure:^(NSURLResponse *response, NSError *error) {
            NSLog(@"%@", response);
            NSLog(@"%@", error);
            
            XCTFail(@"There was an error.");
        }];
    } timeout:30.0];
}

- (void)testSearchHistoryLimit {
    YelpSearchHistory *history = [[YelpSearchHistory alloc] init];
    history.maxNumberOfCollections = 4;
    
    // fetch just the first page
    NSString *term = @"Satan";
    NSString *location = @"Santa Ana, CA";
    
    CREATE_WEAK_SELF(wSelf);
    YelpSearchResponseCollection *oldie = [[YelpSearchResponseCollection alloc] init];
    [self performAsyncTestWithBlock:^{
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:location term:term];
        [[YelpRequest requestForWeedMapsTest] GETSearch:parm success:^(NSDictionary *JSON) {
            XCTAssert(JSON, @"There should be a response object");
            [oldie addSearchResponse:[[YelpSearchResponse alloc] initWithJSON:JSON] parm:parm];
            
            // can't verify this because a business object does not guarantee an image URL
            //YelpSearchResponse *response = [[YelpSearchResponse alloc] initWithJSON:JSON];
            //XCTAssert([response.imageURLs[0] isEqual:_imageURLs[page*YelpSearchParm.businessesPerRequest]], @"These should be the same");
            
            [wSelf asyncTestCompleted];
        } failure:^(NSURLResponse *response, NSError *error) {
            NSLog(@"%@", response);
            NSLog(@"%@", error);
            
            XCTFail(@"There was an error.");
        }];
    } timeout:30.0];
    
    [history addYelpSearchResponseCollection:oldie];
    [history addYelpSearchResponseCollection:[[YelpSearchResponseCollection alloc] init]];
    [history addYelpSearchResponseCollection:[[YelpSearchResponseCollection alloc] init]];
    [history addYelpSearchResponseCollection:[[YelpSearchResponseCollection alloc] init]];
    [history addYelpSearchResponseCollection:[[YelpSearchResponseCollection alloc] init]];

    XCTAssert([history searchResponseCollections].count == 4, @"There should only be 4 in here.");
    
    YelpSearchResponseCollection *fetchedResponse = [history searchResponseCollectionForTerm:term location:location];
    XCTAssert(fetchedResponse == nil, @"Oldie shouldn't be in there.");
}

- (void)testSearchHistoryFetching {
    YelpSearchHistory *history = [[YelpSearchHistory alloc] init];
    
    // fetch just the first page
    NSString *term = @"Satan";
    NSString *location = @"Santa Ana, CA";
    
    CREATE_WEAK_SELF(wSelf);
    YelpSearchResponseCollection *responseCollection = [[YelpSearchResponseCollection alloc] init];
    [self performAsyncTestWithBlock:^{
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:location term:term];
        [[YelpRequest requestForWeedMapsTest] GETSearch:parm success:^(NSDictionary *JSON) {
            XCTAssert(JSON, @"There should be a response object");
            [responseCollection addSearchResponse:[[YelpSearchResponse alloc] initWithJSON:JSON] parm:parm];
            
            // can't verify this because a business object does not guarantee an image URL
            //YelpSearchResponse *response = [[YelpSearchResponse alloc] initWithJSON:JSON];
            //XCTAssert([response.imageURLs[0] isEqual:_imageURLs[page*YelpSearchParm.businessesPerRequest]], @"These should be the same");
            
            [wSelf asyncTestCompleted];
        } failure:^(NSURLResponse *response, NSError *error) {
            NSLog(@"%@", response);
            NSLog(@"%@", error);
            
            XCTFail(@"There was an error.");
        }];
    } timeout:30.0];
    
    // add it to the history
    [history addYelpSearchResponseCollection:responseCollection];
    
    _pageCount = 0;
    YelpSearchResponseCollection *allBusinessesResponseCollection = [[YelpSearchResponseCollection alloc] init];
    [self performAsyncTestWithBlock:^{
        YelpRequest *request = [YelpRequest requestForWeedMapsTest];
        YelpSearchParm *parm = [YelpSearchParm parmWithLocation:@"Santa Ana, CA" term:@"Satan"];
        
        // recursively pull this down
        [self GETSearchForRequest:request parm:parm success:^(NSDictionary *JSON){
            [allBusinessesResponseCollection addSearchResponse:[[YelpSearchResponse alloc] initWithJSON:JSON] parm:parm];
        }];
    } timeout:500.0];
    [history addYelpSearchResponseCollection:allBusinessesResponseCollection];
    
    YelpSearchResponseCollection *target = [history searchResponseCollectionForTerm:term location:location];
    NSLog(@"responseCollection.fetchedBusinesses.count:%lu",                 (unsigned long)responseCollection.fetchedBusinesses.count);
    NSLog(@"allBusinessesResponseCollection.fetchedBusinesses.count:%lu",    (unsigned long)allBusinessesResponseCollection.fetchedBusinesses.count);
    NSLog(@"target:%lu", (unsigned long)target.fetchedBusinesses.count);
    
    XCTAssert(target.fetchedBusinesses.count == allBusinessesResponseCollection.fetchedBusinesses.count, @"This should have fetched us the allBusinessesResponseCollection");
}
@end
