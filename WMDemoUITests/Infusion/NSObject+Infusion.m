//
//  NSObject+Infusion.m
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+Infusion.h"
#import "IAssembly_Private.h"

static char kAssemblyKey;

@implementation NSObject (Infusion)

- (IAssembly*) assembly
{
    @synchronized ( _objectCategorySync )
    {
        IAssembly* assembly = objc_getAssociatedObject(self, &kAssemblyKey);
        if ( assembly )
            return assembly;
        else
            return [[IAssembly alloc] init];
    }
}

- (void) setAssembly: (IAssembly*) assembly
{
    @synchronized ( _objectCategorySync )
    {
        objc_setAssociatedObject(self, &kAssemblyKey, assembly, OBJC_ASSOCIATION_RETAIN);
    }
}

- (IAssembly*) assemblyNamed: (NSString*) assemblyName
{
    // Synchronized in IAssembly
    return [IAssembly lookupAssemblyNamed: assemblyName];
}

@end
