//
//  IFactory_Private.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/8/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"

@class IAssembly;

@interface IFactory ()
{
    IAssembly* __weak _assembly;
}

@property (nonatomic, readwrite, weak) IAssembly* assembly;

@end
