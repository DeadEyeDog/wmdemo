//
//  Infusion.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Infusion.h"
#import "InfusionDefs.h"
#import "IFactory.h"
#import "IAssembly.h"
