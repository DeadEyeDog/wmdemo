//
//  InfusionDefs.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum infusionScope {
    infusionScope_unique,
    infusionScope_assembly,
    infusionScope_singleton,
    //meta
    infusionScope_count,
    infusionScope_unknown
} infusionScope_t;

typedef id(^IConstructorBlock)(NSError*__autoreleasing* error);

extern NSString* const IUnsupportedScopeConstantException;
extern NSString* const IInstanceDoesNotImplementProtocolException;
extern NSString* const IBadParameterException;
extern NSString* const IInstanceWithLongLifetimeAlreadyExistsException;
extern NSString* const IConfigurationException;
