//
//  IFactory.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InfusionDefs.h"

@interface IFactory : NSObject <NSCopying>

@property (nonatomic, readonly) Protocol* protocol;

// Convenience constructors
+ (instancetype) factoryWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor defaultScope: (infusionScope_t) scope;
+ (instancetype) factoryWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor;

///// This is the designated initializer /////
- (instancetype) initWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor defaultScope: (infusionScope_t) scope;
//////////////////////////////////////////////

- (instancetype) initWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor;

// Creating instances
- (id) instanceWithScope: (infusionScope_t) scope error: (NSError*__autoreleasing*) error;
- (id) instanceWithScope: (infusionScope_t) scope;
- (id) instance;

// Comparing factories - WARNING: this only tests if the protocol and the default scope are the
// same. It's not possible to compare blocks, so this could be wrong! I only have it here because
// it facilitates unit testing in some cases where I don't care about the actual blocks.
- (BOOL) isEqualToFactory: (IFactory*) factory;

@end
