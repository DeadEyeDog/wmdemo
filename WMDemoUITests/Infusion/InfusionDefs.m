//
//  InfusionDefs.m
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "InfusionDefs.h"

NSString* const IUnsupportedScopeConstantException = @"IUnsupportedScopeConstantException";
NSString* const IInstanceDoesNotImplementProtocolException = @"IInstanceDoesNotImplementProtocolException";
NSString* const IBadParameterException = @"IBadParameterException";
NSString* const IInstanceWithLongLifetimeAlreadyExistsException = @"IInstanceWithLongLifetimeAlreadyExistsException";
NSString* const IConfigurationException = @"IConfigurationException";
