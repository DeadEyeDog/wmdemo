//
//  IAssembly.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InfusionDefs.h"

// Forward Declaration of IFactory.
@class IFactory;


// IAssembly Interface
@interface IAssembly : NSObject <NSCopying>

@property (nonatomic, readwrite) NSString* name;

+ (instancetype) assemblyWithName: (NSString*) name;
- (instancetype) initWithName: (NSString*) name;

- (void) addFactory: (IFactory*) factory;
- (IFactory*) factoryForType: (Protocol*) protocol;

// Comparing assemblies
- (BOOL) isEqualToAssembly: (IAssembly*) assembly;

@end
