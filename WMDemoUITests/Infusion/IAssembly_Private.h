//
//  IAssembly_Private.h
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IAssembly.h"

extern NSObject* _objectCategorySync;

@interface IAssembly ()

+ (IAssembly*) lookupAssemblyNamed: (NSString*) name;

+ (void) setSingletonScopeInstance: (id) instance forType: (Protocol*) protocol;
+ (id) singletonScopeInstanceForType: (Protocol*) protocol;

- (void) setAssemblyScopeInstance: (id) instance forType: (Protocol*) protocol;
- (id) assemblyScopeInstanceForType: (Protocol*) protocol;

@end
