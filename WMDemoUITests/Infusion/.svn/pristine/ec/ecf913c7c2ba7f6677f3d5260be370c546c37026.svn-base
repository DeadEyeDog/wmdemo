//
//  IAssembly.m
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IAssembly_Private.h"
#import "IFactory_Private.h"

// Used to synchronize internal state
static NSObject* _assembyInternalSync = nil;

// Used to synchronize NSObject category access
NSObject* _objectCategorySync = nil;

static NSMutableDictionary* _assemblyMap = nil;
static NSMutableDictionary* _singletonScopeInstanceMap = nil;

static NSString* const kDefaultAssemblyName = @"com.studiopmg.Infusion.DefaultAssemblyName";

@interface IAssembly ()
{
    NSString* _name;
    NSMutableDictionary* _factoryMap;
    NSMutableDictionary* _assemblyScopeInstanceMap;
}

@end

@implementation IAssembly

@synthesize name = _name;

#pragma mark
#pragma mark Initialization

+ (void) initialize
{
    if ( self == [IAssembly class] )
    {
        // Data
        _assemblyMap = [NSMutableDictionary dictionary];
        _singletonScopeInstanceMap = [NSMutableDictionary dictionary];
        
        // Sync
        _assembyInternalSync = [[NSObject alloc] init];
        _objectCategorySync = [[NSObject alloc] init];
    }
}

+ (instancetype) assemblyWithName: (NSString*) name
{
    return [[self alloc] initWithName: name];
}

- (instancetype) initWithName: (NSString*) name
{
    self = [super init];
    if ( self )
    {
        @synchronized ( _assembyInternalSync )
        {
            _name = [name copy];
            _factoryMap = [NSMutableDictionary dictionary];
            _assemblyScopeInstanceMap = [NSMutableDictionary dictionary];
            _assemblyMap[name] = self;
        }
    }
    
    return self;
}

- (instancetype) init
{
    @synchronized ( _assembyInternalSync )
    {
        if ( _assemblyMap[kDefaultAssemblyName] )
        {
            return _assemblyMap[kDefaultAssemblyName];
        }
        else
        {
            return [self initWithName: kDefaultAssemblyName];
        }
    }
}

#pragma mark
#pragma mark Public Interface

- (void) setName: (NSString*) name
{
    if ( name == nil )
    {
        @throw [NSException exceptionWithName: IBadParameterException
                                       reason: @"Assembly names can not be nil."
                                     userInfo: nil];
    }
    
    @synchronized ( _assembyInternalSync )
    {
        if ( [_name isEqualToString: name] )
            return;
        
        // Add self under new name
        _assemblyMap[name] = self;
        
        // Remove self from under old name
        [_assemblyMap removeObjectForKey: _name];
        
        // Set new name
        _name = [name copy];
    }
}

- (void) addFactory: (IFactory*) factory
{
    @synchronized ( _assembyInternalSync )
    {
        NSString* protocolString = NSStringFromProtocol(factory.protocol);
        _factoryMap[protocolString] = factory;
        factory.assembly = self;
    }
}

- (IFactory*) factoryForType: (Protocol*) protocol
{
    @synchronized ( _assembyInternalSync )
    {
        NSString* protocolString = NSStringFromProtocol(protocol);
        
        return _factoryMap[protocolString];
    }
}

- (BOOL) isEqualToAssembly: (IAssembly*) assembly
{
    NSSet* myKeys = [NSSet setWithArray: _factoryMap.allKeys];
    NSSet* theirKeys = [NSSet setWithArray: assembly->_factoryMap.allKeys];
    
    if ( ![myKeys isEqualToSet: theirKeys] )
        return NO;
    
    BOOL equal = YES;
    for ( NSString* key in myKeys )
    {
        IFactory* myFactory = _factoryMap[key];
        IFactory* theirFactory = assembly->_factoryMap[key];
        
        if ( ![myFactory isEqualToFactory: theirFactory] )
        {
            equal = NO;
            break;
        }
    }
    
    return equal;
}

#pragma mark
#pragma mark Implementation

- (id) copyWithZone: (NSZone*) zone
{
    @synchronized ( _assembyInternalSync )
    {
        NSString* newName = [NSString stringWithFormat: @"(%@ copy)", _name];
        
        IAssembly* newAssembly = [[[self class] allocWithZone: zone] initWithName: newName];
        
        for ( IFactory* factory in _factoryMap.allValues )
        {
            IFactory* newFactory = [factory copyWithZone: zone];
            [newAssembly addFactory: newFactory];
        }
        
        return newAssembly;
    }
}

- (BOOL) isEqual: (id) object
{
    if ( self == object )
        return YES;
    
    if ( ![object isKindOfClass: [self class]] )
        return NO;
    
    return [self isEqualToAssembly: (IAssembly*)object];
}

- (NSUInteger) hash
{
    NSUInteger hash = 0UL;
    
    for ( IFactory* factory in _factoryMap.allValues )
    {
        hash ^= factory.hash;
    }
    
    return hash;
}

#pragma mark
#pragma mark Private Interface

+ (IAssembly*) lookupAssemblyNamed: (NSString*) name
{
    @synchronized ( _assembyInternalSync )
    {
        return _assemblyMap[name];
    }
}

- (void) setAssemblyScopeInstance: (id) instance forType: (Protocol*) protocol
{
    @synchronized ( _assembyInternalSync )
    {
        NSString* protocolKey = NSStringFromProtocol(protocol);
    
        if ( _assemblyScopeInstanceMap[protocolKey] != nil )
        {
            @throw [NSException exceptionWithName: IInstanceWithLongLifetimeAlreadyExistsException
                                           reason: @"An instance with a long lifetime has already been created for the type \"%@\"."
                                         userInfo: nil];
        }
        
        _assemblyScopeInstanceMap[protocolKey] = instance;
    }
}

- (id) assemblyScopeInstanceForType: (Protocol*) protocol
{
    id instance = nil;
    
    @synchronized ( _assembyInternalSync )
    {
        NSString* protocolKey = NSStringFromProtocol(protocol);
    
        instance = _assemblyScopeInstanceMap[protocolKey];
    }
    
    return instance;
}

+ (void) setSingletonScopeInstance: (id) instance forType: (Protocol*) protocol
{
    @synchronized( _assembyInternalSync )
    {
        NSString* protocolKey = NSStringFromProtocol(protocol);
    
        if ( _singletonScopeInstanceMap[protocolKey] != nil )
        {
            @throw [NSException exceptionWithName: IInstanceWithLongLifetimeAlreadyExistsException
                                           reason: @"An instance with a long lifetime has already been created for the type \"%@\"."
                                         userInfo: nil];
        }
        
        _singletonScopeInstanceMap[protocolKey] = instance;
    }
}

+ (id) singletonScopeInstanceForType: (Protocol*) protocol
{
    id instance = nil;
    
    @synchronized ( _assembyInternalSync )
    {
        NSString* protocolKey = NSStringFromProtocol(protocol);

        instance = _singletonScopeInstanceMap[protocolKey];
    }
    
    return instance;
}

@end
