//
//  IFactory.m
//  Infusion
//
//  Created by Gustavo Di Martino on 8/7/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory_Private.h"
#import "IAssembly_Private.h"

@interface IFactory ()
{
    IConstructorBlock _constructorBlock;
    infusionScope_t _defaultScope;
    Protocol* _protocol;
}

@end

@implementation IFactory

@synthesize protocol = _protocol;
@synthesize assembly = _assembly;

+ (instancetype) factoryWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor defaultScope: (infusionScope_t) scope
{
    return [[self alloc] initWithType: protocol constructor: constructor defaultScope: scope];
}

+ (instancetype) factoryWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor
{
    return [[self alloc] initWithType: protocol constructor: constructor];
}

- (instancetype) initWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor defaultScope: (infusionScope_t) scope
{
    self = [super init];
    if ( self )
    {
        _protocol = protocol;
        _constructorBlock = [constructor copy];
        _defaultScope = scope;
    }
    
    return self;
}

- (instancetype) initWithType: (Protocol*) protocol constructor: (IConstructorBlock) constructor
{
    return [self initWithType: protocol constructor: constructor defaultScope: infusionScope_unique];
}

- (id) copyWithZone: (NSZone*) zone
{
    return [[[self class] allocWithZone: zone] initWithType: _protocol
                                                constructor: [_constructorBlock copy]
                                               defaultScope: _defaultScope];
}

- (id) instanceWithScope: (infusionScope_t) scope error: (NSError*__autoreleasing*) error
{
    if ( _assembly == nil )
    {
        @throw [NSException exceptionWithName: IConfigurationException
                                       reason: @"Factories must be added to an assembly before they can be used."
                                     userInfo: nil];
    }
    
    id instance = nil;
    
    switch ( scope ) {
        case infusionScope_unique:
        {
            instance = _constructorBlock(error);
            break;
        }
        case infusionScope_assembly:
        {
            instance = [_assembly assemblyScopeInstanceForType: _protocol];
            if ( instance == nil )
            {
                instance = _constructorBlock(error);
                if ( instance )
                    [_assembly setAssemblyScopeInstance: instance forType: _protocol];
            }
            break;
        }
        case infusionScope_singleton:
        {
            instance = [IAssembly singletonScopeInstanceForType: _protocol];
            if ( instance == nil )
            {
                instance = _constructorBlock(error);
                if ( instance )
                    [IAssembly setSingletonScopeInstance: instance forType: _protocol];
            }
            break;
        }
        default:
        {
            NSString* reason = [NSString stringWithFormat: @"Unsupported scope constant in switch statement: %d.", scope];
            @throw [NSException exceptionWithName: IUnsupportedScopeConstantException
                                           reason: reason
                                         userInfo: nil];
            break;
        }
    }
    
    if ( instance && ![instance conformsToProtocol: _protocol] )
    {
        NSString* reason = [NSString stringWithFormat: @"Instance of \"%@\" does not conform to protocol \"%@\".", NSStringFromClass([instance class]), NSStringFromProtocol(_protocol)];
        @throw [NSException exceptionWithName: IInstanceDoesNotImplementProtocolException
                                       reason: reason
                                     userInfo: nil];
    }
    
    return instance;
}

- (id) instanceWithScope: (infusionScope_t) scope
{
    return [self instanceWithScope: scope error: nil];
}

- (id) instance
{
    return [self instanceWithScope: _defaultScope];
}

- (BOOL) isEqualToFactory: (IFactory*) factory
{
    if ( factory == nil )
        return NO;
    
    NSString* myProtocolString = NSStringFromProtocol(_protocol);
    NSString* theirProtocalString = NSStringFromProtocol(factory.protocol);
    if ( ![myProtocolString isEqualToString: theirProtocalString] )
        return NO;

    if ( _defaultScope != factory->_defaultScope )
        return NO;
    
    return YES;
}

- (BOOL) isEqual: (id) object
{
    if ( self == object )
        return YES;
    
    if ( ![object isKindOfClass: [self class]] )
        return NO;
    
    return [self isEqualToFactory: (IFactory*) object];
}

- (NSUInteger) hash
{
    NSString* protocolString = NSStringFromProtocol(_protocol);
    NSUInteger protocolHash = [protocolString hash];
    
    return protocolHash ^ _defaultScope;
}

@end
