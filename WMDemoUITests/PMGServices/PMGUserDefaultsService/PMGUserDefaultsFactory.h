//
//  PMGUserDefaultsFactory.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 10/29/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"

@interface PMGUserDefaultsFactory : IFactory

@end
