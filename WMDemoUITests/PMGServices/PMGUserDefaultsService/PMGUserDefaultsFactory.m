//
//  PMGUserDefaultsFactory.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 10/29/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGUserDefaultsFactory.h"
#import "PMGUserDefaultsServiceImp.h"

@implementation PMGUserDefaultsFactory

- (instancetype) init
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        return [[PMGUserDefaultsServiceImp alloc] init];
    };
    
    return [super initWithType: @protocol(PMGUserDefaultsService)
                   constructor: constructorBlock
                  defaultScope: infusionScope_singleton];
}

@end
