//
//  PMGUserDefaultsServiceImp.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 10/29/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGUserDefaultsServiceImp.h"
#import "PMGLoggingService.h"
#import "Infusion.h"

NSString* const PMGUserDefaultsKeyMustBeAStringException = @"PMGUserDefaultsKeyMustBeAStringException";

@interface PMGUserDefaultsServiceImp ()
{
    NSMutableDictionary* _lastKnown;
    BOOL _programmatic;
    void (^_defaultsDidChangeCallback)();
}

@end

@implementation PMGUserDefaultsServiceImp

- (instancetype) init
{
    self = [super init];
    if ( self )
    {
        _lastKnown = [NSMutableDictionary dictionary];
        
        _programmatic = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(userDefaultsDidChange:)
                                                     name: NSUserDefaultsDidChangeNotification
                                                   object: nil];
    }
    
    return self;
}

- (BOOL) supportsOptionalInterface
{
    return YES;
}

- (id) objectForKeyedSubscript: (id) key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: key];
}

- (void) setObject: (id) object forKeyedSubscript: (id <NSCopying, NSObject>) key
{
    NSString* stringKey = nil;
    if ( [key isKindOfClass: [NSString class]] )
    {
        stringKey = (NSString*)key;
    }
    else
    {
        @throw [NSException exceptionWithName: PMGUserDefaultsKeyMustBeAStringException
                                       reason: @"The key must be a string value."
                                     userInfo: nil];
    }
    
    _programmatic = YES;

    if ( object )
    {
        [[NSUserDefaults standardUserDefaults] setObject: object forKey: stringKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _lastKnown[stringKey] = object;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: stringKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _lastKnown[key] = [NSNull null];
    }
    
    _programmatic = NO;
}

- (void) removeObjectForKey: (NSString*) key
{
    _programmatic = YES;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _lastKnown[key] = [NSNull null];
    
    _programmatic = NO;
}

- (void) registerDefaults: (NSDictionary*) defaults
{
    [[NSUserDefaults standardUserDefaults] registerDefaults: defaults];
    
    for ( NSString* key in [defaults allKeys] )
    {
        NSObject* obj = [[NSUserDefaults standardUserDefaults] objectForKey: key];
        if ( obj )
            _lastKnown[key] = obj;
        else
            _lastKnown[key] = defaults[key];
    }
}

- (void) userDefaultsDidChange: (NSNotification*) notification
{
    if ( _programmatic )
        return;
    
    if ( _defaultsDidChangeCallback )
    {
        BOOL changeDetected = NO;
        for ( NSString* key in [_lastKnown allKeys] )
        {
            NSObject* newValue = [[NSUserDefaults standardUserDefaults] objectForKey: key];
            if ( newValue == nil )
                newValue = [NSNull null];
            
            NSObject* oldValue = _lastKnown[key];
            if ( oldValue == nil )
                oldValue = [NSNull null];
            
            if ( ![newValue isEqual: oldValue] )
            {
                [instanceOf(PMGLoggingService) logInfo: F(@"User defaults change detected: key = \"%@\", old value = \"%@\", new value = \"%@\".", key, oldValue, newValue)];
                _lastKnown[key] = newValue;
                changeDetected = YES;
            }
        }
        
        if ( changeDetected )
            _defaultsDidChangeCallback();
    }
}

- (void) setUserDefaultsDidChangeInBackgroundCallback: (void(^)()) callback
{
    _defaultsDidChangeCallback = [callback copy];
}

@end
