//
//  PMGUserDefaultsService.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 10/29/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

// Possible Exceptions
extern NSString* const PMGUserDefaultsKeyMustBeAStringException;

@protocol PMGUserDefaultsService <NSObject>

@property (nonatomic, readonly) BOOL supportsOptionalInterface;

- (id) objectForKeyedSubscript: (id) key;
- (void) setObject: (id) object forKeyedSubscript: (id <NSCopying>) key;
- (void) removeObjectForKey: (NSString*) key;

// OPTIONAL INTERFACE:
// This part of the protocol is declared as optional so that if you create
// a testing version of this service, you will not have to do as much work.
// If you decide to support it, be sure to return YES for the
// "supportsOptionalInterface" property above.
@optional
// Allows the client to specify a block to call in the event that
// a user preference is changed while the app is in the background.
- (void) setUserDefaultsDidChangeInBackgroundCallback: (void(^)()) callback;

// Does the same as the method by the same name in NSUserDefaults. I just have
// it here so that the client doesn't have to mix and match calls to this
// class verses calls to the actual NSUserDefaults.
- (void) registerDefaults: (NSDictionary*) defaults;

@end
