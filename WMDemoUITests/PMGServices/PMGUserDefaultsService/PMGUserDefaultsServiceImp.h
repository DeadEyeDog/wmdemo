//
//  PMGUserDefaultsServiceImp.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 10/29/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGUserDefaultsService.h"

@interface PMGUserDefaultsServiceImp : NSObject <PMGUserDefaultsService>

@end
