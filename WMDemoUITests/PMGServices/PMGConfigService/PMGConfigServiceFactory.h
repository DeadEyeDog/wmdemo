//
//  PMGConfigServiceFactory.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 9/10/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"

@interface PMGConfigServiceFactory : IFactory

@end
