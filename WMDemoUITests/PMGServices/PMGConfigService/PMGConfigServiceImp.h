//
//  PMGConfigServiceImp.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 9/9/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGConfigService.h"

@interface PMGConfigServiceImp : NSObject <PMGConfigService>

@end
