//
//  PMGConfigServiceImp.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 9/9/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGConfigServiceImp.h"

NSString* const PMGConfigServiceErrorDomain = @"com.studiopmg.PMGServices.PMGConfigServiceErrorDomain";
NSString* const PMGConfigServiceNotInitializedException = @"PMGConfigServiceNotInitializedException";
NSString* const PMGConfigServiceNotOnMainThreadException = @"PMGConfigServiceNotOnMainThreadException";
NSString* const PMGConfigServiceKeyNotFoundException = @"PMGConfigServiceKeyNotFoundException";
NSString* const PMGConfigServiceIncorrectDataTypeException = @"PMGConfigServiceIncorrectDataTypeException";
NSString* const PMGConfigServiceInvalidProfileException = @"PMGConfigServiceInvalidProfileException";

static NSString* const kConfigFileBaseName = @"config";
static NSString* const kConfigFileExtension = @"json";

@interface PMGConfigServiceImp ()
{
    NSString* _defaultProfileName;
    NSString* _currentProfileName;
    NSDictionary* _configMap;
    NSDictionary* _defaultsMap;
    NSDictionary* _globalMap;
    NSArray* _profileNames;
    BOOL _initialized;
}

@end

@implementation PMGConfigServiceImp

#pragma mark - Initialization

- (instancetype) init
{
    self = [super init];
    if ( self )
    {
        // Only basic initialization should go here
        _initialized = NO;
    }
    
    return self;
}

- (BOOL) initializeServiceError: (NSError* __autoreleasing*) error
{
    if ( ![NSThread isMainThread] )
    {
        if ( error )
        {
            NSString* description = @"The methods in this service may only be called from the main thread.";
            *error = [NSError errorWithDomain: PMGConfigServiceErrorDomain
                                         code: pmgConfigServiceError_notOnMainThread
                                     userInfo: @{NSLocalizedDescriptionKey: description}];
        }
        
        return NO;
    }
    
    id rootObject = [self rootObjectFromJSONFileError: error];
    if ( rootObject == nil )
    {
        return NO;
    }
    
    if ( ![rootObject isKindOfClass: [NSDictionary class]] )
    {
        if ( error )
        {
            *error = [self formattingError: @"The root object of a config file must be a dictionary."];
        }
        
        return NO;
    }
    
    NSDictionary* rootDict = (NSDictionary*) rootObject;
    _defaultProfileName = rootDict[@"defaultProfile"];
    if ( _defaultProfileName == nil )
    {
        if ( error )
        {
            *error = [self formattingError: @"A config file must include a \"defaultProfile\" key."];
        }
        
        return NO;
    }
    
    _currentProfileName = [_defaultProfileName copy];
    
    id globalObject = rootDict[@"global"];
    if ( globalObject )
    {
        if ( ![globalObject isKindOfClass: [NSDictionary class]] )
        {
            if ( error )
            {
                *error = [self formattingError: @"A \"global\" section is not required, but if present it must be a dictionary."];
            }
            
            return NO;
        }
        
        _globalMap = [globalObject copy];
    }
    
    NSArray* profiles = rootDict[@"profiles"];
    if ( profiles == nil || ![profiles isKindOfClass: [NSArray class]] )
    {
        if ( error )
        {
            *error = [self formattingError: @"A \"profiles\" section is required and must be an array."];
        }
        
        return NO;
    }
    
    NSMutableArray* mutableProfileNames = [NSMutableArray array];
    NSMutableDictionary* mutableConfigMap = [NSMutableDictionary dictionary];
    for ( int i = 0; i < profiles.count; i++ )
    {
        id objectThatShouldBeADictionary = profiles[i];
        if ( [objectThatShouldBeADictionary isKindOfClass: [NSDictionary class]] )
        {
            NSDictionary* dict = (NSDictionary*) objectThatShouldBeADictionary;
            [mutableProfileNames addObject: dict[@"name"]];
            mutableConfigMap[dict[@"name"]] = dict;
        }
        else
        {
            if ( error )
            {
                *error = [self formattingError: @"The \"profiles\" array must contain only dictionaries."];
            }
            
            return NO;
        }
    }
    
    _profileNames = [mutableProfileNames copy];
    _configMap = [mutableConfigMap copy];
    
    // Make sure there is a profile with the current name.
    NSDictionary* testDict = _configMap[_currentProfileName];
    if ( testDict == nil )
    {
        if ( error )
        {
            NSString* message = [NSString stringWithFormat: @"The current profile is set to \"%@\", but no profile by that name could be found.", _currentProfileName];
            *error = [self formattingError: message];
        }
        
        return NO;
    }

    _initialized = YES;
    
    return YES;
}

- (void) setDefaults: (NSDictionary*) defaults
{
    [self checkForProgrammerErrors];
    
    _defaultsMap = [defaults copy];
}

#pragma mark - Public Interface

- (NSString*) defaultProfileName
{
    [self checkForProgrammerErrors];
    
    return _defaultProfileName;
}

- (NSArray*) allProfileNames
{
    [self checkForProgrammerErrors];
    
    return [_profileNames copy];
}

- (void) setCurrentProfileName: (NSString*) currentProfile
{
    [self checkForProgrammerErrors];
    
    NSDictionary* testDict = _configMap[currentProfile];
    if ( testDict == nil )
    {
        NSString* reason = [NSString stringWithFormat: @"No profile by the name \"%@\" exist.", currentProfile];
        @throw [NSException exceptionWithName: PMGConfigServiceInvalidProfileException
                                       reason: reason
                                     userInfo: nil];
    }
    
    _currentProfileName = [currentProfile copy];
}

- (NSString*) currentProfileName
{
    [self checkForProgrammerErrors];
    
    return _currentProfileName;
}

- (NSString*) stringForKey: (NSString*) key
{
    [self checkForProgrammerErrors];
    
    id obj = [self objectForKey: key];
    if ( ![obj isKindOfClass: [NSString class]] )
    {
        NSString* reason = [NSString stringWithFormat: @"The value for \"%@\" is not an NSString.", key];
        @throw [NSException exceptionWithName: PMGConfigServiceIncorrectDataTypeException
                                       reason: reason
                                     userInfo: nil];
    }
    
    return obj;
}

- (BOOL) boolForKey: (NSString*) key
{
    [self checkForProgrammerErrors];
    
    id obj = [self objectForKey: key];
    if ( ![obj isKindOfClass: [NSNumber class]] )
    {
        NSString* reason = [NSString stringWithFormat: @"The value for \"%@\" is not an NSNumber (BOOLS are stored as NSNumbers).", key];
        @throw [NSException exceptionWithName: PMGConfigServiceIncorrectDataTypeException
                                       reason: reason
                                     userInfo: nil];
    }

    return [obj boolValue];
}

- (NSInteger) integerForKey: (NSString*) key
{
    [self checkForProgrammerErrors];
    
    id obj = [self objectForKey: key];
    if ( ![obj isKindOfClass: [NSNumber class]] )
    {
        NSString* reason = [NSString stringWithFormat: @"The value for \"%@\" is not an NSNumber (NSIntegers are stored as NSNumbers).", key];
        @throw [NSException exceptionWithName: PMGConfigServiceIncorrectDataTypeException
                                       reason: reason
                                     userInfo: nil];
    }
    
    return [obj integerValue];
}

- (float) floatForKey: (NSString*) key
{
    [self checkForProgrammerErrors];
    
    id obj = [self objectForKey: key];
    if ( ![obj isKindOfClass: [NSNumber class]] )
    {
        NSString* reason = [NSString stringWithFormat: @"The value for \"%@\" is not an NSNumber (floats are stored as NSNumbers).", key];
        @throw [NSException exceptionWithName: PMGConfigServiceIncorrectDataTypeException
                                       reason: reason
                                     userInfo: nil];
    }
    
    return [obj floatValue];
}

- (double) doubleForKey: (NSString*) key
{
    [self checkForProgrammerErrors];
    
    id obj = [self objectForKey: key];
    if ( ![obj isKindOfClass: [NSNumber class]] )
    {
        NSString* reason = [NSString stringWithFormat: @"The value for \"%@\" is not an NSNumber (doubles are stored as NSNumbers).", key];
        @throw [NSException exceptionWithName: PMGConfigServiceIncorrectDataTypeException
                                       reason: reason
                                     userInfo: nil];
    }
    
    return [obj doubleValue];
}

#pragma mark - Implementation

- (id) objectForKey: (NSString*) key
{
    NSDictionary* profileDict = _configMap[_currentProfileName];
    id object = profileDict[key];
    if ( object == nil )
    {
        object = _globalMap[key];
        if ( object == nil )
        {
            object = _defaultsMap[key];
            if ( object == nil )
            {
                NSString* reason = [NSString stringWithFormat: @"The key \"%@\" could not be found. Please be sure to call [PMGConfigService setDefaults:] if you want to support missing keys.", key];
                @throw [NSException exceptionWithName: PMGConfigServiceKeyNotFoundException
                                               reason: reason
                                             userInfo: nil];
            }
        }
    }
    
    return object;
}

- (void) checkForProgrammerErrors
{
    if ( ![NSThread isMainThread] )
    {
        @throw [NSException exceptionWithName: PMGConfigServiceNotOnMainThreadException
                                       reason: @"The methods in this service may only be called from the main thread."
                                     userInfo: nil];
    }
    
    if ( !_initialized )
    {
        @throw [NSException exceptionWithName: PMGConfigServiceNotInitializedException
                                       reason: @"The method \"[PMGConfigService initializeServiceError:]\" must be called prior to accessing any part of this service."
                                     userInfo: nil];
    }
}

- (NSError*) formattingError: (NSString*) message
{
    NSString* description = [NSString stringWithFormat: @"The file \"%@.%@\" is incorrectly formatted. %@", kConfigFileBaseName, kConfigFileExtension, message];

    return [NSError errorWithDomain: PMGConfigServiceErrorDomain
                               code: pmgConfigServiceError_configFileIncorrectlyFormatted
                           userInfo: @{ NSLocalizedDescriptionKey: description }];
}

- (id) rootObjectFromJSONFileError: (NSError* __autoreleasing*) error
{
    NSString* fullPath = [[NSBundle mainBundle] pathForResource: kConfigFileBaseName ofType: kConfigFileExtension];
    if ( fullPath == nil )
    {
        if ( error )
        {
            NSString* description = [NSString stringWithFormat: @"Couldn't find \"%@.%@\" file in application bundle.", kConfigFileBaseName, kConfigFileExtension];
            *error = [NSError errorWithDomain: PMGConfigServiceErrorDomain
                                         code: pmgConfigServiceError_configFileNotFound
                                     userInfo: @{ NSLocalizedDescriptionKey: description }];
        }

        return nil;
    }
    
    NSData* jsonData = [NSData dataWithContentsOfFile: fullPath];
    if ( jsonData == nil )
    {
        if ( error )
        {
            NSString* description = [NSString stringWithFormat: @"The file \"%@.%@\" appears to be corrupt.", kConfigFileBaseName, kConfigFileExtension];
            *error = [NSError errorWithDomain: PMGConfigServiceErrorDomain
                                         code: pmgConfigServiceError_configFileCorrupt
                                     userInfo: @{ NSLocalizedDescriptionKey: description }];
        }
        
        return nil;
    }
    
    id rootObject = [NSJSONSerialization JSONObjectWithData: jsonData options: 0 error: error];
    if ( rootObject == nil )
    {
        return nil;
    }
    
    return rootObject;
}

@end
