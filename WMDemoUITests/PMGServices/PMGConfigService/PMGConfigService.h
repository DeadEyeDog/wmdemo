//
//  PMGConfigService.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 9/9/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

// Possible Errors
typedef enum pmgConfigServiceError
{
    pmgConfigServiceError_none,
    pmgConfigServiceError_configFileNotFound,
    pmgConfigServiceError_configFileCorrupt,
    pmgConfigServiceError_configFileIncorrectlyFormatted,
    pmgConfigServiceError_notOnMainThread,
    //meta
    pmgConfigServiceError_count,
    pmgConfigServiceError_invalid
} pmgConfigServiceError_t;

extern NSString* const PMGConfigServiceErrorDomain;

// Possible Exceptions
extern NSString* const PMGConfigServiceNotInitializedException;
extern NSString* const PMGConfigServiceNotOnMainThreadException;
extern NSString* const PMGConfigServiceKeyNotFoundException;
extern NSString* const PMGConfigServiceIncorrectDataTypeException;
extern NSString* const PMGConfigServiceInvalidProfileException;

@protocol PMGConfigService <NSObject>

@property (nonatomic, readonly) NSString* defaultProfileName;
@property (nonatomic, readonly) NSArray* allProfileNames;
@property (nonatomic, assign) NSString* currentProfileName;

- (BOOL) initializeServiceError: (NSError* __autoreleasing*) error;
- (void) setDefaults: (NSDictionary*) defaults;

- (NSString*) stringForKey: (NSString*) key;
- (BOOL) boolForKey: (NSString*) key;
- (NSInteger) integerForKey: (NSString*) key;
- (float) floatForKey: (NSString*) key;
- (double) doubleForKey: (NSString*) key;

@end
