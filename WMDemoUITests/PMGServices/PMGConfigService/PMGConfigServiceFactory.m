//
//  PMGConfigServiceFactory.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 9/10/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGConfigServiceFactory.h"
#import "PMGConfigServiceImp.h"

@implementation PMGConfigServiceFactory

- (instancetype) init
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        id result = nil;
        
        PMGConfigServiceImp* configService = [[PMGConfigServiceImp alloc] init];
        if ( [configService initializeServiceError: error] )
            result = configService;
        
        return result;
    };
    
    return [super initWithType: @protocol(PMGConfigService)
                   constructor: constructorBlock
                  defaultScope: infusionScope_singleton];
}

@end
