//
//  PMGReachabilityFactory.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGReachabilityFactory.h"
#import "PMGReachabilityServiceImp.h"

@implementation PMGReachabilityFactory

- (instancetype) init
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        return [[PMGReachabilityServiceImp alloc] init];
    };
    
    return [super initWithType: @protocol(PMGReachabilityService)
                   constructor: constructorBlock
                  defaultScope: infusionScope_singleton];
}

@end
