//
//  PMGReachabilityServiceImp.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/14/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGReachabilityService.h"

@interface PMGReachabilityServiceImp : NSObject <PMGReachabilityService>

@end
