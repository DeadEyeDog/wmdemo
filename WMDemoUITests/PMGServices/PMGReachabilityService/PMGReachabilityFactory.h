//
//  PMGReachabilityFactory.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"

@interface PMGReachabilityFactory : IFactory

@end
