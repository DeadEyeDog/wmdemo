//
//  PMGReachabilityService.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/14/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const PMGReachabilityDidChange;
extern NSString* const PMGReachabilityHostKey;
extern NSString* const PMGReachabilityReachableKey;
extern NSString* const PMGReachabilityWWANKey;

@protocol PMGReachabilityService <NSObject>

@property (atomic, readonly) NSArray* trackedHosts;
@property (atomic) BOOL userAllowsWWANConnections;

- (void) addHostToTrack: (NSString*) host;
- (void) stopTrackingHost: (NSString*) host;
- (BOOL) hostIsReachable: (NSString*) host;
- (BOOL) hostIsReachableViaWWAN: (NSString*) host;

@end
