//
//  PMGReachabilityServiceImp.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/14/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <objc/runtime.h>
#import "PMGReachabilityServiceImp.h"
#import "Infusion.h"
#import "PMGLoggingService.h"
#import "TMReachability.h"
#import "SafeNotification.h"

static NSObject* _syncObj = nil;
static char kHostNameKey;

NSString* const PMGReachabilityDidChange = @"PMGReachabilityDidChange";
NSString* const PMGReachabilityHostKey = @"PMGReachabilityHostKey";
NSString* const PMGReachabilityReachableKey = @"PMGReachabilityReachableKey";
NSString* const PMGReachabilityWWANKey = @"PMGReachabilityWWANKey";

@interface PMGReachabilityServiceImp ()
{
    NSMutableDictionary* _hostMap;
    BOOL _userAllowsWWANConnections;
}

@end

@implementation PMGReachabilityServiceImp

@synthesize userAllowsWWANConnections = _userAllowsWWANConnections;

+ (void) initialize
{
    if ( self == [PMGReachabilityServiceImp class] )
    {
        _syncObj = [[NSObject alloc] init];
    }
}

- (instancetype) init
{
    self = [super init];
    if ( self )
    {
        _hostMap = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (NSArray*) trackedHosts
{
    NSArray* trackedHosts;
    
    @synchronized ( _syncObj )
    {
        trackedHosts = [_hostMap allKeys];
    }
    
    return trackedHosts;
}

- (void) addHostToTrack: (NSString*) host
{
    @synchronized ( _syncObj )
    {
        TMReachability* reachability = _hostMap[host];
        if ( reachability )
            return;
        
        reachability = [TMReachability reachabilityWithHostName: host];
        objc_setAssociatedObject(reachability, &kHostNameKey, [host copy], OBJC_ASSOCIATION_RETAIN);
        
        reachability.reachableBlock = ^(TMReachability* notifyingReachability)
        {
            [self handleReachabilityChanged: notifyingReachability];
        };

        reachability.unreachableBlock = ^(TMReachability* notifyingReachability)
        {
            [self handleReachabilityChanged: notifyingReachability];
        };

        [reachability startNotifier];
        
        _hostMap[host] = reachability;
    }
}

- (void) handleReachabilityChanged: (TMReachability*) reachability
{
    NSString* associatedHost = objc_getAssociatedObject(reachability, &kHostNameKey);
    BOOL isReachable = reachability.isReachable;
    BOOL viaWWAN = reachability.isReachableViaWWAN;

    dispatch_async(dispatch_get_main_queue(), ^{
        [instanceOf(PMGLoggingService) logInfo: F(@"Reachability changed: host = \"%@\", reachable = %@, viaWWAN = %@",
                                                  associatedHost,
                                                  isReachable ? @"YES": @"NO",
                                                  viaWWAN ? @"YES" : @"NO")];
        
        SafeNotification* notification = [SafeNotification notificationWithName: PMGReachabilityDidChange object: self];
        notification[PMGReachabilityHostKey] = associatedHost;
        notification[PMGReachabilityReachableKey] = [NSNumber numberWithBool: isReachable];
        notification[PMGReachabilityWWANKey] = [NSNumber numberWithBool: viaWWAN];
        [notification notify];
    });
}

- (void) stopTrackingHost: (NSString*) host
{
    @synchronized ( _syncObj )
    {
        TMReachability* reachability = _hostMap[host];
        if ( reachability == nil )
            return;
        
        [reachability stopNotifier];
        
        [_hostMap removeObjectForKey: host];
    }
}

- (BOOL) hostIsReachable: (NSString*) host
{
    BOOL isReachable = NO;
    
    @synchronized ( _syncObj )
    {
        TMReachability* reachability = _hostMap[host];
        if ( reachability )
            isReachable = reachability.isReachable;
    }
    
    return isReachable;
}

- (BOOL) hostIsReachableViaWWAN: (NSString*) host
{
    BOOL isReachable = NO;
    
    @synchronized ( _syncObj )
    {
        TMReachability* reachability = _hostMap[host];
        if ( reachability )
            isReachable = reachability.isReachableViaWWAN;
    }
    
    return isReachable;
}

@end
