//
//  PMGLoggerFactory.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "PMGLoggerFactory.h"
#import "PMGLoggingServiceImp.h"

@implementation PMGLoggerFactory

- (instancetype) init
{
    return [self initOutputToConsole: YES];
}

- (instancetype) initWithVerbosity: (verbosity_t) verbosity
{
    return [self initWithVerbosity: verbosity outputToConsole: YES];
}

- (instancetype) initOutputToConsole: (BOOL) outputToConsole
{
    return [self initWithVerbosity: verbosity_info outputToConsole: outputToConsole];
}

- (instancetype) initWithVerbosity: (verbosity_t) verbosity
                   outputToConsole: (BOOL) outputToConsole
{
    return [self initWithMaxBackupLogs: 10
                        maxLogFileSize: 3ull * 1024ull * 1024ull
                             verbosity: verbosity
                       outputToConsole: outputToConsole];
}

// Designated Initializer
- (instancetype) initWithMaxBackupLogs: (NSInteger) maxBackupLogs
                        maxLogFileSize: (unsigned long long) maxLogFileSize
                             verbosity: (verbosity_t) verbosity
                       outputToConsole: (BOOL) outputToConsole
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        PMGLoggingServiceImp* logger = [[PMGLoggingServiceImp alloc] init];
        
        logger.maxBackupLogs = maxBackupLogs;
        logger.maxLogFileSize = maxLogFileSize;
        logger.verbosity = verbosity;
        logger.outputToConsole = outputToConsole;

        return logger;
    };
    
    return [super initWithType: @protocol(PMGLoggingService)
                   constructor: constructorBlock
                  defaultScope: infusionScope_singleton];
}

@end
