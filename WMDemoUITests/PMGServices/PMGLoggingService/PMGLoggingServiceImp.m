//
//  PMGLoggingServiceImp.m
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <string.h>
#import "PMGLoggingServiceImp.h"
#import "ZipSupport.h"

NSString* const PMGLoggingErrorDomain = @"com.studiopmg.PMGServices.PMGLoggingErrorDomain";

static NSObject* _syncObj = nil;
static const int kMaxBuff = 256;

@interface PMGLoggingServiceImp ()
{
    NSFileHandle* _logFileHandle;
    NSDateFormatter* _logFormatter;
    NSDateFormatter* _fileNameFormatter;
    BOOL _logFileOpen;
    unsigned long long _maxLogFileSize;
    NSInteger _maxBackupLogs;
    BOOL _outputToConsole;
    verbosity_t _verbosity;
    NSString* _appName;
    NSString* _appVersion;
    NSString* _buildNumber;
    NSString* _deviceName;
    const char _cAppName[kMaxBuff + 1];
    const char _cAppVersion[kMaxBuff + 1];
    const char _cBuildNumber[kMaxBuff + 1];
    const char _cDeviceName[kMaxBuff + 1];
}

@property (nonatomic, readonly) NSString* appName;
@property (nonatomic, readonly) NSString* appVersion;
@property (nonatomic, readonly) NSString* buildNumber;
@property (nonatomic, readonly) const char* cAppName;
@property (nonatomic, readonly) const char* cAppVersion;
@property (nonatomic, readonly) const char* cBuildNumber;
@property (nonatomic, readonly) const char* cDeviceName;

@end

@implementation PMGLoggingServiceImp

#pragma mark
#pragma mark Initialization/Cleanup

+ (void) initialize
{
    if ( self == [PMGLoggingServiceImp class] )
    {
        _syncObj = [[NSObject alloc] init];
    }
}

- (instancetype) init
{
    self = [super init];
    if ( self )
    {
        _maxLogFileSize = 3ull * 1024ull * 1024ull; // 3MB
        _maxBackupLogs = 10;
        _verbosity = verbosity_info;

        _logFormatter = [[NSDateFormatter alloc] init];
        NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US_POSIX"];
        [_logFormatter setLocale: locale];
        [_logFormatter setDateFormat: @"yyyyMMdd.HH:mm:ss"];
        [_logFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"America/Los_Angeles"]];
        
        _fileNameFormatter = [[NSDateFormatter alloc] init];
        [_fileNameFormatter setLocale: locale];
        [_fileNameFormatter setDateFormat: @"yyyyMMdd_HHmmss"];
        [_fileNameFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"America/Los_Angeles"]];
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(applicationDidEnterBackground:)
                                                     name: UIApplicationDidEnterBackgroundNotification
                                                   object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(applicationWillEnterForeground:)
                                                     name: UIApplicationWillEnterForegroundNotification
                                                   object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(applicationWillTerminate:)
                                                     name: UIApplicationWillTerminateNotification
                                                   object: nil];
        
        memset((void*)_cAppName, 0, sizeof(_cAppName));
        memset((void*)_cAppVersion, 0, sizeof(_cAppVersion));
        memset((void*)_cBuildNumber, 0, sizeof(_cBuildNumber));
        memset((void*)_cDeviceName, 0, sizeof(_cDeviceName));
    }
    
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

#pragma mark
#pragma mark Public Interface

@synthesize maxLogFileSize = _maxLogFileSize;
@synthesize maxBackupLogs = _maxBackupLogs;
@synthesize outputToConsole = _outputToConsole;
@synthesize verbosity = _verbosity;

- (BOOL) supportsOptionalInterface
{
    return YES;
}

- (void) logError: (NSString*) format, ...
{
    @synchronized ( _syncObj )
    {
        if ( _verbosity < verbosity_error )
            return;
    }
    
    va_list args;
    va_start(args, format);
    
    NSString* newFormat = [NSString stringWithFormat: @"[ERROR] %@", format];
    [self logFormat: newFormat arguments: args forceConsole: YES];
    
    va_end(args);
}

- (void) logWarning: (NSString*) format, ...
{
    @synchronized ( _syncObj )
    {
        if ( _verbosity < verbosity_warning )
            return;
    }
    
    va_list args;
    va_start(args, format);
    
    NSString* newFormat = [NSString stringWithFormat: @"[WARNING] %@", format];
    [self logFormat: newFormat arguments: args forceConsole: NO];
    
    va_end(args);
}

- (void) logInfo: (NSString*) format, ...
{
    @synchronized ( _syncObj )
    {
        if ( _verbosity < verbosity_info )
            return;
    }
    
    va_list args;
    va_start(args, format);
    
    NSString* newFormat = [NSString stringWithFormat: @"[INFO] %@", format];
    [self logFormat: newFormat arguments: args forceConsole: NO];
    
    va_end(args);
}

- (void) logDebug: (NSString*) format, ...
{
    @synchronized ( _syncObj )
    {
        if ( _verbosity < verbosity_debug )
            return;
    }
    
    va_list args;
    va_start(args, format);
    
    NSString* newFormat = [NSString stringWithFormat: @"[DEBUG] %@", format];
    [self logFormat: newFormat arguments: args forceConsole: NO];
    
    va_end(args);
}

- (void) logVerbose: (NSString*) format, ...
{
    @synchronized ( _syncObj )
    {
        if ( _verbosity < verbosity_verbose )
            return;
    }
    
    va_list args;
    va_start(args, format);
    
    NSString* newFormat = [NSString stringWithFormat: @"[VERBOSE] %@", format];
    [self logFormat: newFormat arguments: args forceConsole: NO];
    
    va_end(args);
}

#pragma mark
#pragma mark Implementation

- (NSString*) appName
{
    if ( _appName == nil )
    {
        NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
        
        _appName = [info objectForKey: (NSString*)kCFBundleNameKey];
    }
    
    return _appName;
}

- (NSString*) appVersion
{
    if ( _appVersion == nil )
    {
        NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
        
        _appVersion = [info objectForKey: @"CFBundleShortVersionString"];
    }
    
    return _appVersion;
}

- (NSString*) buildNumber
{
    if ( _buildNumber == nil )
    {
        NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
        
        _buildNumber = [info objectForKey: @"CFBundleVersion"];
    }
    
    return _buildNumber;
}

- (const char*) cAppName
{
    if ( strlen(_cAppName) == 0 )
    {
        const char* pText = [self.appName UTF8String];
        strncpy((char*)_cAppName, pText, kMaxBuff);
    }
    
    return _cAppName;
}

- (const char*) cAppVersion
{
    if ( strlen(_cAppVersion) == 0 )
    {
        const char* pText = [self.appVersion UTF8String];
        strncpy((char*)_cAppVersion, pText, kMaxBuff);
    }
    
    return _cAppVersion;
}

- (const char*) cBuildNumber
{
    if ( strlen(_cBuildNumber) == 0 )
    {
        const char* pText = [self.buildNumber UTF8String];
        strncpy((char*)_cBuildNumber, pText, kMaxBuff);
    }
    
    return _cBuildNumber;
}

- (const char*) cDeviceName
{
    if ( strlen(_cDeviceName) == 0 )
    {
        const char* pText = [[[UIDevice currentDevice] name] UTF8String];
        strncpy((char*)_cDeviceName, pText, kMaxBuff);
    }
    
    return _cDeviceName;
}

- (void) logFormat: (NSString*) format arguments: (va_list) args forceConsole: (BOOL) forceConsole
{
    NSString* timestamp = [_logFormatter stringFromDate: [NSDate date]];
    const char* ts = [timestamp UTF8String];
    
    NSString* messageString = [[NSString alloc] initWithFormat: format arguments: args];
    const char* message = [messageString UTF8String];

    @synchronized ( _syncObj )
    {
        if ( _outputToConsole || forceConsole )
            fprintf(stderr, "%s [%s(v%s b%s):%s] %s\n", ts, self.cAppName, self.cAppVersion, self.cBuildNumber, self.cDeviceName, message);
        
        if ( _logFileOpen == NO )
            [self openLogFile];
        
        dprintf(_logFileHandle.fileDescriptor, "%s [%s(v%s b%s):%s] %s\n", ts, self.cAppName, self.cAppVersion, self.cBuildNumber, self.cDeviceName, message);
        
        if ( _logFileHandle.offsetInFile > _maxLogFileSize )
            [self closeLogFile]; // next time a message is logged, the log file will be cycled...
    }
}

- (void) applicationDidEnterBackground: (NSNotification*) notification
{
    NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
    NSNumber* option = [info objectForKey: @"UIApplicationExitsOnSuspend"];
    BOOL exitsOnSuspend = [option boolValue];
    if ( exitsOnSuspend == NO )
        [self closeLogFile];
}

- (void) applicationWillEnterForeground: (NSNotification*) notification
{
    [self openLogFile];
}

- (void) applicationWillTerminate: (NSNotification*) notification
{
    [self closeLogFile];
}

- (NSString*) createZipFile
{
    NSString* zipFileName;
    BOOL success;
    
    @synchronized ( _syncObj ) // stop any activity that might lead to changes in the number of logs
    {
        [self closeLogFile];
        
        zipFileName = [self uniqueZipFilePath];
        
        ZipArchive* archive = [[ZipArchive alloc] init];
        success = [archive CreateZipFile2: zipFileName];
        if ( success )
        {
            NSArray* logFiles = [self backupLogFiles];
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            for ( NSString* fileName in logFiles )
            {
                NSString* fullPath = [documentsPath stringByAppendingPathComponent: fileName];
                success = [archive addFileToZip: fullPath newname: fileName];
                if ( !success )
                    break;
            }
            
            if ( success )
                success = [archive addFileToZip: [self logFilePath] newname: [[self logFilePath] lastPathComponent]];
            
            [archive CloseZipFile2];
        }
    }
    
    return success ? zipFileName : nil;
}

- (NSString*) createZipArchiveOfLogs: (NSError*__autoreleasing*) error
{
    NSString* pathToZipFile = [self createZipFile];
    if ( pathToZipFile == nil && error != nil )
    {
        *error = [NSError errorWithDomain: PMGLoggingErrorDomain
                                     code: pmgLoggingError_failedToCreateArchive
                                 userInfo: @{NSLocalizedDescriptionKey: @"Failed to create the log file archive."}];
    }
    
    return pathToZipFile;
}

- (void) openLogFile
{
    @synchronized ( _syncObj )
    {
        if ( _logFileOpen )
            return;
        
        [self archiveLogIfNeeded];
        
        if ( [[NSFileManager defaultManager] fileExistsAtPath: [self logFilePath]] == NO )
        {
            [[NSFileManager defaultManager] createFileAtPath: [self logFilePath]
                                                    contents: nil
                                                  attributes: nil];
        }
        
        _logFileHandle = [NSFileHandle fileHandleForWritingAtPath: [self logFilePath]];
        [_logFileHandle seekToEndOfFile];
        
        _logFileOpen = YES;
    }
}

- (void) closeLogFile
{
    @synchronized ( _syncObj )
    {
        if ( _logFileOpen )
        {
            _logFileOpen = NO;
            
            [_logFileHandle closeFile];
        }
    }
}

- (void) archiveLogIfNeeded
{
    @synchronized ( _syncObj ) // already synched where this is called, but just in case this ends up getting called elsewhere...
    {
        // See if the current log has gotten too big. If so, move it to a backup file.
        unsigned long long fileSize = 0ull;
        
        NSDictionary* attributes = [[NSFileManager defaultManager] attributesOfItemAtPath: [self logFilePath] error: nil];
        if ( attributes )
            fileSize = [attributes fileSize];
        
        if ( fileSize > _maxLogFileSize )
            [[NSFileManager defaultManager] moveItemAtPath: [self logFilePath] toPath: [self backupFilePath] error: nil];
        
        // Check if the backup log files have accumulated past the max allowed number. If so, then delete some until
        // the number of files is equal to the limit.
        NSMutableArray* logFiles = [NSMutableArray arrayWithArray: [self backupLogFiles]];
        if ( logFiles.count > _maxBackupLogs )
        {
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
            
            [logFiles sortUsingSelector: @selector(compare:)];
            
            while ( logFiles.count > _maxBackupLogs )
            {
                NSString* fileName = [logFiles objectAtIndex: 0];
                NSString* fullPath = [documentsPath stringByAppendingPathComponent: fileName];
                [[NSFileManager defaultManager] removeItemAtPath: fullPath error: nil];
                [logFiles removeObjectAtIndex: 0];
            }
        }
    }
}

- (NSArray*) backupLogFiles
{
    NSMutableArray* logFiles = [NSMutableArray array];

    @synchronized ( _syncObj ) // contents of folder might change
    {
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSArray* directoryListing = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: documentsPath error: nil];
        NSString* prefix = [NSString stringWithFormat: @"%@_%@_", self.appName, [[UIDevice currentDevice] name]];
        for ( NSString* fileName in directoryListing )
        {
            if ( [fileName hasPrefix: prefix] && [fileName hasSuffix: @".log"] )
                [logFiles addObject: fileName];
        }
    }
    
    return [logFiles copy];
}

- (NSString*) logFilePath
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* fileName = [NSString stringWithFormat: @"%@_%@.log", self.appName, [[UIDevice currentDevice] name]];
    
    return [documentsPath stringByAppendingPathComponent: fileName];
}

- (NSString*) backupFilePath
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* timestamp = [_fileNameFormatter stringFromDate: [NSDate date]];
    NSString* fileName = [NSString stringWithFormat: @"%@_%@_%@.log", self.appName, [[UIDevice currentDevice] name], timestamp];
    
    return [documentsPath stringByAppendingPathComponent: fileName];
}

- (NSString*) uniqueZipFilePath
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString* timestamp = [_fileNameFormatter stringFromDate: [NSDate date]];
    NSString* fileName = [NSString stringWithFormat: @"%@_%@_%@.zip", self.appName, [[UIDevice currentDevice] name], timestamp];
    
    return [documentsPath stringByAppendingPathComponent: fileName];
}

@end
