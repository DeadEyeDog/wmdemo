//
//  PMGLoggingServiceImp.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGLoggingService.h"

@interface PMGLoggingServiceImp : NSObject <PMGLoggingService>

@end
