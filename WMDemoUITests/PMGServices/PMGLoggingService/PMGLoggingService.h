//
//  PMGLoggingService.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/14/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>

// Convenience macro that adds filename and line number info to your log output.
// USAGE EXAMPLE:
// [logger logInfo: F(@"Saving file: %@", myFileName)];
#define F(format...) @"(%@:%d) %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat: format]

typedef enum verbosity
{
    verbosity_silent,
    verbosity_error,
    verbosity_warning,
    verbosity_info,
    verbosity_debug,
    verbosity_verbose,
    //meta
    verbosity_count,
    verbosity_invalid
} verbosity_t;

typedef enum pmgLoggingError
{
    pmgLoggingError_none,
    pmgLoggingError_badArguments,
    pmgLoggingError_failedToCreateArchive,
    //meta
    pmgLoggingError_count,
    pmgLoggingError_invalid
} pmgLoggingError_t;

extern NSString* const PMGLoggingErrorDomain;

@protocol PMGLoggingService <NSObject>

@property (atomic) verbosity_t verbosity;
@property (nonatomic, readonly) BOOL supportsOptionalInterface; // see below

- (void) logError: (NSString*) format, ...;
- (void) logWarning: (NSString*) format, ...;
- (void) logInfo: (NSString*) format, ...;
- (void) logDebug: (NSString*) format, ...;
- (void) logVerbose: (NSString*) format, ...;

// OPTIONAL INTERFACE:
// This is the optional interface which primarily provides rolling log support.
// It is recommended that you support this if you create your own implementation
// of this interface, but it's not required. If you support it, be sure to return
// YES for the "supportsOptionalInterface" property above.
@optional

@property (atomic) unsigned long long maxLogFileSize;
@property (atomic) NSInteger maxBackupLogs;
@property (atomic) BOOL outputToConsole;

// Zips up the logs and returns the path to the newly created zip file.
// Caller must delete the zip file when finished with it. Returns nil if
// the zip file couldn't be created. Check error for more info in this
// case.
- (NSString*) createZipArchiveOfLogs: (NSError*__autoreleasing*) error;

@end
