//
//  PMGLoggerFactory.h
//  PMGServices
//
//  Created by Gustavo Di Martino on 8/15/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"
#import "PMGLoggingService.h"

@interface PMGLoggerFactory : IFactory

- (instancetype) initWithVerbosity: (verbosity_t) verbosity;

- (instancetype) initOutputToConsole: (BOOL) outputToConsole;

- (instancetype) initWithVerbosity: (verbosity_t) verbosity
                   outputToConsole: (BOOL) outputToConsole;

// Designated Initializer
- (instancetype) initWithMaxBackupLogs: (NSInteger) maxBackupLogs
                        maxLogFileSize: (unsigned long long) maxLogFileSize
                             verbosity: (verbosity_t) verbosity
                       outputToConsole: (BOOL) outputToConsole;

@end
