//
//  ZTLoggerFactory.h
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "IFactory.h"

@interface ZTLoggerFactory : IFactory

@end
