//
//  FileMD5Hash.c
//  ZippyTest
//
//  Created by Gustavo Di Martino on 7/25/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//
// This code comes from the following website:
// https://www.joel.lopes-da-silva.com/2010/09/07/compute-md5-or-sha-hash-of-large-file-efficiently-on-ios-and-mac-os-x/
//

#include <stdint.h>
#include <stdio.h>
#include <CoreFoundation/CoreFoundation.h>
#include <CommonCrypto/CommonDigest.h>

#define FileHashDefaultChunkSizeForReadingData 4096

CFStringRef FileMD5HashCreateWithPath(CFStringRef filePath, size_t chunkSizeForReadingData)
{
    CFStringRef result = NULL;
    CFReadStreamRef readStream = NULL;
    
    // Get the file URL
    CFURLRef fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)filePath, kCFURLPOSIXPathStyle, (Boolean)false);
    if ( !fileURL )
        goto done;
    
    // Create and open the read stream
    readStream = CFReadStreamCreateWithFile(kCFAllocatorDefault, (CFURLRef)fileURL);
    if ( !readStream )
        goto done;
    
    bool didSucceed = (bool)CFReadStreamOpen(readStream);
    if ( !didSucceed )
        goto done;
    
    // Make sure chunkSizeForReadingData is valid
    if ( !chunkSizeForReadingData )
        chunkSizeForReadingData = FileHashDefaultChunkSizeForReadingData;
    
    // Initialize the hash object
    CC_MD5_CTX hashObject;
    CC_MD5_Init(&hashObject);
    
    // Feed the data to the hash object
    bool hasMoreData = true;
    while ( hasMoreData )
    {
        uint8_t buffer[chunkSizeForReadingData];
        CFIndex readBytesCount = CFReadStreamRead(readStream, (UInt8*)buffer, (CFIndex)sizeof(buffer));
        if ( readBytesCount == -1 )
            break;
        
        if ( readBytesCount == 0 )
        {
            hasMoreData = false;
            continue;
        }
        
        CC_MD5_Update(&hashObject, (const void*)buffer, (CC_LONG)readBytesCount);
    }
    
    // Check if the read operation succeeded
    didSucceed = !hasMoreData;
    
    // Compute the hash digest
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &hashObject);
    
    // Abort if the read operation failed
    if ( !didSucceed )
        goto done;
    
    // Compute the string result
    char hash[2 * sizeof(digest) + 1];
    for ( size_t i = 0; i < sizeof(digest); ++i )
    {
        snprintf(hash + (2 * i), 3, "%02x", (int)(digest[i]));
    }
    
    result = CFStringCreateWithCString(kCFAllocatorDefault, (const char*)hash, kCFStringEncodingUTF8);
    
done:
    if ( readStream )
    {
        CFReadStreamClose(readStream);
        CFRelease(readStream);
    }
    
    if ( fileURL )
    {
        CFRelease(fileURL);
    }
    
    return result;
}