//
//  ZTLogger.m
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "ZTLogger.h"

@interface ZTLogger ()
{
    verbosity_t _verbosity;
}

@end

@implementation ZTLogger

@synthesize verbosity = _verbosity;

- (BOOL) supportsOptionalInterface
{
    return NO;
}

- (void) logError: (NSString*) format, ...
{
    // no-op
}

- (void) logWarning: (NSString*) format, ...
{
    // no-op
}

- (void) logInfo: (NSString*) format, ...
{
    // no-op
}

- (void) logDebug: (NSString*) format, ...
{
    // no-op
}

- (void) logVerbose: (NSString*) format, ...
{
    // no-op
}

@end
