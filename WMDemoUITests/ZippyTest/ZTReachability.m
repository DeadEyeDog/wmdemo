//
//  ZTReachability.m
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "ZTReachability.h"

@interface ZTReachability ()
{
    NSMutableSet* _hostsToTrack;
    NSMutableSet* _reachableHosts;
    NSMutableSet* _reachableViaWWANHosts;
    BOOL _userAllowsWWANConnections;
}

@end

@implementation ZTReachability

@synthesize userAllowsWWANConnections = _userAllowsWWANConnections;

- (instancetype) init
{
    self = [super init];
    if ( self )
    {
        _hostsToTrack = [NSMutableSet set];
        _reachableHosts = [NSMutableSet set];
        _reachableViaWWANHosts = [NSMutableSet set];
    }
    
    return self;
}

- (void) addHostToTrack: (NSString*) host
{
    [_hostsToTrack addObject: host];
}

- (NSArray*) trackedHosts
{
    return [_hostsToTrack allObjects];
}

- (void) stopTrackingHost: (NSString*) host
{
    [_hostsToTrack removeObject: host];
    [_reachableHosts removeObject: host];
    [_reachableViaWWANHosts removeObject: host];
}

- (BOOL) hostIsReachable: (NSString*) host
{
    return [_reachableHosts containsObject: host] ? YES : NO;
}

- (BOOL) hostIsReachableViaWWAN: (NSString*) host
{
    return [_reachableViaWWANHosts containsObject: host] ? YES : NO;
}

- (void) setHostIsReachable: (NSString*) host
{
    if ( [_hostsToTrack containsObject: host] )
    {
        [_reachableHosts addObject: host];
    }
}

- (void) setHostIsUnreachable: (NSString*) host
{
    [_reachableHosts removeObject: host];
    [_reachableViaWWANHosts removeObject: host];
}

- (void) setHostIsReachableViaWWAN: (NSString*) host
{
    if ( [_hostsToTrack containsObject: host] )
    {
        [_reachableHosts addObject: host];
        [_reachableViaWWANHosts addObject: host];
    }
}

- (void) setHostIsUnreachableViaWWAN: (NSString*) host
{
    [_reachableViaWWANHosts removeObject: host];
}

@end
