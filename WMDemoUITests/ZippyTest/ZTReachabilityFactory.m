//
//  ZTReachabilityFactory.m
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "ZTReachabilityFactory.h"
#import "ZTReachability.h"

@implementation ZTReachabilityFactory

- (instancetype) init
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        ZTReachability* reachability = [[ZTReachability alloc] init];
        
        return reachability;
    };
    
    return [super initWithType: @protocol(PMGReachabilityService)
                   constructor: constructorBlock
                  defaultScope: infusionScope_singleton];
}

@end
