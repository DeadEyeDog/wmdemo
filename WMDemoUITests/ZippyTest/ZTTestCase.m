//
//  ZTTestCase.m
//  ZippyTest
//
//  Created by Gustavo Di Martino on 7/22/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "ZTTestCase.h"
#import "FileMD5Hash.h"

@interface ZTTestCase ()
{
    dispatch_semaphore_t _asyncTestSemaphore;
    NSMutableOrderedSet* _conditions;
    BOOL _conditionOrderIsSignificant;
    BOOL _multipleConditionTestIsRunning;
    BOOL _testIsInErrorState;
}

@end

@implementation ZTTestCase

- (void) setUp
{
    [super setUp];
    
    _conditions = [NSMutableOrderedSet orderedSet];
    _conditionOrderIsSignificant = NO;
}

- (void) tearDown
{
    _conditions = nil;
    
    [super tearDown];
}

- (NSString*) testFolderPath
{
    NSString* className = NSStringFromClass([self class]);
    NSString* rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString* testingPath = [rootPath stringByAppendingPathComponent: @"UnitTests"];
    NSString* testingFolder = [testingPath stringByAppendingPathComponent: className];
    
    if ( [[NSFileManager defaultManager] fileExistsAtPath: testingFolder] == NO )
    {
        NSError* error;
        BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath: testingFolder
                                                 withIntermediateDirectories: YES
                                                                  attributes: nil
                                                                       error: &error];
        XCTAssert(success, @"Failed to create folder: \"%@\". Error description: \"%@\".", testingFolder, error.localizedDescription);
        if ( !success )
            return nil;
    }
    
    return testingFolder;
}

- (NSString*) testPathForFileWithName: (NSString*) fileName
{
    return [self.testFolderPath stringByAppendingPathComponent: fileName];
}

- (void) clearTestFolder
{
    NSError* error;
    NSArray* contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: self.testFolderPath error: &error];
    XCTAssert(contents, @"Failed to retrieve contents of folder: \"%@\". Error description: \"%@\".", self.testFolderPath, error.localizedDescription);
    
    if ( contents )
    {
        for ( NSString* name in contents )
        {
            NSString* path = [self.testFolderPath stringByAppendingPathComponent: name];
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath: path error: &error];
            NSAssert(success, @"Failed to remove item: \"%@\". Error description: \"%@\".", name, error.localizedDescription);
        }
    }
}

- (NSString*) md5HashForFileWithPath: (NSString*) fullPath
{
    CFStringRef md5hash = FileMD5HashCreateWithPath((__bridge CFStringRef)fullPath, 4096);
    
    return CFBridgingRelease(md5hash);
}

- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout
{
    _asyncTestSemaphore = dispatch_semaphore_create(0);

    testBlock();
    
    NSDate* start = [NSDate date];
    while ( dispatch_semaphore_wait(_asyncTestSemaphore, DISPATCH_TIME_NOW) )
    {
        BOOL success = [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
        XCTAssert(success, @"The run loop exited because there are no input sources.");
        
        NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: start];
        if ( elapsed > timeout )
        {
            XCTFail(@"*** asynchronous test timed out ***");
            dispatch_semaphore_signal(_asyncTestSemaphore);
        }
    };
}

- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout
                     callbackAfter: (NSTimeInterval) secondsTillCallback
                     callbackBlock: (void(^)()) callbackBlock
{
    _asyncTestSemaphore = dispatch_semaphore_create(0);
    
    testBlock();
    
    BOOL firedCallback = NO;
    NSDate* start = [NSDate date];
    while ( dispatch_semaphore_wait(_asyncTestSemaphore, DISPATCH_TIME_NOW) )
    {
        BOOL success = [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode
                                                beforeDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
        XCTAssert(success, @"The run loop exited because there are no input sources.");
        
        NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: start];
        if ( elapsed > timeout )
        {
            XCTFail(@"*** asynchronous test timed out ***");
            dispatch_semaphore_signal(_asyncTestSemaphore);
        }
        else if ( elapsed > secondsTillCallback && !firedCallback )
        {
            callbackBlock();
            firedCallback = YES;
        }
    };
}

- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout
                     callbackEvery: (NSTimeInterval) secondsTillCallback
                     callbackBlock: (void(^)()) callbackBlock
{
    _asyncTestSemaphore = dispatch_semaphore_create(0);
    
    testBlock();
    
    NSDate* start = [NSDate date];
    while ( dispatch_semaphore_wait(_asyncTestSemaphore, DISPATCH_TIME_NOW) )
    {
        BOOL success = [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode
                                                beforeDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
        XCTAssert(success, @"The run loop exited because there are no input sources.");
        
        NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: start];
        if ( elapsed > timeout )
        {
            XCTFail(@"*** asynchronous test timed out ***");
            dispatch_semaphore_signal(_asyncTestSemaphore);
        }
        else if ( elapsed > secondsTillCallback )
        {
            callbackBlock();
        }
    };
}

- (void) asyncTestCompleted
{
    dispatch_semaphore_signal(_asyncTestSemaphore);
}

- (void) addConditionWithName: (NSString*) conditionName
{
    if ( [_conditions containsObject: conditionName] )
    {
        XCTFail(@"A condition must have a unique name.");
        return;
    }
    
    if ( _multipleConditionTestIsRunning )
    {
        XCTFail(@"All conditions must be added before starting the test.");
        return;
    }
    
    [_conditions addObject: conditionName];
}

- (BOOL) containsConditionWithName: (NSString*) conditionName
{
    return [_conditions containsObject: conditionName];
}

- (void) removeConditionWithName: (NSString*) conditionName
{
    [_conditions removeObject: conditionName];
}

- (void) didMeetCondition: (NSString*) conditionName
{
    if ( ![_conditions containsObject: conditionName] )
    {
        XCTFail(@"The condition \"%@\" doesn't exist or has already been met.", conditionName);
        dispatch_semaphore_signal(_asyncTestSemaphore);
        return;
    }
    
    if ( _conditionOrderIsSignificant )
    {
        NSString* nextCondition = [_conditions objectAtIndex: 0];
        if ( [nextCondition isEqualToString: conditionName] )
        {
            [_conditions removeObjectAtIndex: 0];
        }
        else
        {
            XCTFail(@"The condition \"%@\" was met out of order.", conditionName);
            dispatch_semaphore_signal(_asyncTestSemaphore);
            return;
        }
    }
    else
    {
        [_conditions removeObject: conditionName];
    }
    
    if ( _conditions.count == 0 )
        dispatch_semaphore_signal(_asyncTestSemaphore);
}

- (BOOL) conditionOrderIsSignificant
{
    return _conditionOrderIsSignificant;
}

- (void) setConditionOrderIsSignificant: (BOOL) orderIsSignificant
{
    _conditionOrderIsSignificant = orderIsSignificant;
}

- (void) performTestForMultipleConditionsWithBlock: (void(^)()) testBlock
                                           timeout: (NSTimeInterval) timeout
{
    _asyncTestSemaphore = dispatch_semaphore_create(0);
    
    testBlock();
    
    NSDate* start = [NSDate date];
    while ( dispatch_semaphore_wait(_asyncTestSemaphore, DISPATCH_TIME_NOW) )
    {
        BOOL success = [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
        XCTAssert(success, @"The run loop exited because there are no input sources.");
        
        NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: start];
        if ( elapsed > timeout )
        {
            XCTFail(@"*** asynchronous test timed out ***");
            dispatch_semaphore_signal(_asyncTestSemaphore);
        }
    };
}

- (void) performTestForMultipleConditionsWithBlock: (void(^)()) testBlock
                                           timeout: (NSTimeInterval) timeout
                                     callbackAfter: (NSTimeInterval) secondsTillCallback
                                     callbackBlock: (void (^)()) callbackBlock
{
    _asyncTestSemaphore = dispatch_semaphore_create(0);
    
    testBlock();
    
    BOOL firedCallback = NO;
    NSDate* start = [NSDate date];
    while ( dispatch_semaphore_wait(_asyncTestSemaphore, DISPATCH_TIME_NOW) )
    {
        BOOL success = [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode
                                                beforeDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
        XCTAssert(success, @"The run loop exited because there are no input sources.");
        
        NSTimeInterval elapsed = [[NSDate date] timeIntervalSinceDate: start];
        if ( elapsed > timeout )
        {
            XCTFail(@"*** asynchronous test timed out ***");
            dispatch_semaphore_signal(_asyncTestSemaphore);
        }
        else if ( elapsed > secondsTillCallback && !firedCallback )
        {
            callbackBlock();
            firedCallback = YES;
        }
    };
}

@end
