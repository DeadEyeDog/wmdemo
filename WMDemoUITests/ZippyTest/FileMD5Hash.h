//
//  FileMD5Hash.h
//  ZippyTest
//
//  Created by Gustavo Di Martino on 7/25/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//
// This code comes from the following website:
// https://www.joel.lopes-da-silva.com/2010/09/07/compute-md5-or-sha-hash-of-large-file-efficiently-on-ios-and-mac-os-x/
//

#ifndef ZippyTest_FileMD5Hash_h
#define ZippyTest_FileMD5Hash_h

CFStringRef FileMD5HashCreateWithPath(CFStringRef filePath, size_t chunkSizeForReadingData);

#endif
