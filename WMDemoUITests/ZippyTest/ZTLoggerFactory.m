//
//  ZTLoggerFactory.m
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import "ZTLoggerFactory.h"
#import "ZTLogger.h"

@implementation ZTLoggerFactory

- (instancetype) init
{
    IConstructorBlock constructorBlock = ^id(NSError** error) {
        ZTLogger* reachability = [[ZTLogger alloc] init];
        
        // no configuration at this time
        
        return reachability;
    };
    
    return [super initWithType: @protocol(PMGLoggingService) constructor: constructorBlock defaultScope: infusionScope_unique];
}

@end
