//
//  ZTTestCase.h
//  ZippyTest
//
//  Created by Gustavo Di Martino on 7/22/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ZTTestCase : XCTestCase

@property (nonatomic, readonly) NSString* testFolderPath;

// File and Folder Utilities
- (NSString*) testPathForFileWithName: (NSString*) fileName;

- (void) clearTestFolder;

- (NSString*) md5HashForFileWithPath: (NSString*) fullPath;

// Asynchronous Testing
- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout;

- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout
                     callbackAfter: (NSTimeInterval) secondsTillCallback
                     callbackBlock: (void(^)()) callbackBlock;

- (void) performAsyncTestWithBlock: (void(^)()) testBlock
                           timeout: (NSTimeInterval) timeout
                     callbackEvery: (NSTimeInterval) secondsTillCallback
                     callbackBlock: (void(^)()) callbackBlock;

- (void) asyncTestCompleted;

// Testing Code with Multiple Conditions That Need to Be Met
- (void) addConditionWithName: (NSString*) conditionName;
- (BOOL) containsConditionWithName: (NSString*) conditionName;
- (void) removeConditionWithName: (NSString*) conditionName;
- (void) didMeetCondition: (NSString*) conditionName;
- (BOOL) conditionOrderIsSignificant;
- (void) setConditionOrderIsSignificant: (BOOL) orderIsSignificant;
- (void) performTestForMultipleConditionsWithBlock: (void(^)()) testBlock
                                           timeout: (NSTimeInterval) timeout;
- (void) performTestForMultipleConditionsWithBlock: (void(^)()) testBlock
                                           timeout: (NSTimeInterval) timeout
                                     callbackAfter: (NSTimeInterval) secondsTillCallback
                                     callbackBlock: (void(^)()) callbackBlock;

@end
