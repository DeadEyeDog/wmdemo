//
//  ZTLogger.h
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGLoggingService.h"

@interface ZTLogger : NSObject <PMGLoggingService>

@end
