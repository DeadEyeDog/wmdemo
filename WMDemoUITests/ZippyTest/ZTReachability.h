//
//  ZTReachability.h
//  ZippyTest
//
//  Created by Gustavo Di Martino on 8/18/14.
//  Copyright (c) 2014 StudioPMG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMGReachabilityService.h"

@interface ZTReachability : NSObject <PMGReachabilityService>

// Not part of the standard reachability interface. These are used
// to configure your test reachability service to fake reachability
// to any host you want.
- (void) setHostIsReachable: (NSString*) host;
- (void) setHostIsUnreachable: (NSString*) host;
- (void) setHostIsReachableViaWWAN: (NSString*) host;
- (void) setHostIsUnreachableViaWWAN: (NSString*) host;

@end
