//
//  SafeNotification.h
//  SafeNotification
//
//  Created by Gustavo Di Martino on 1/30/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#include <UIKit/UIKit.h>

@interface SafeNotification : NSObject

@property (nonatomic, readonly) NSString* name;
@property (nonatomic, strong) id object;
@property (nonatomic) BOOL performInlineIfPossible;

+ (SafeNotification*) notificationWithName: (NSString*) name;
+ (SafeNotification*) notificationWithName: (NSString*) name object: (id) object;

- (id) initWithName: (NSString*) name;
- (id) initWithName: (NSString*) name object: (id) object;

- (id) objectForKeyedSubscript: (id) key;
- (void) setObject: (id) object forKeyedSubscript: (id < NSCopying >) key;

- (void) setObject: (id) object forKey: (id < NSCopying >) key;
- (void) setValue: (id) value forKey: (NSString*) key;

- (void) removeObjectForKey: (id) key;
- (void) removeAllObjects;

- (void) notify;

@end
