//
//  SafeNotification.m
//  SafeNotification
//
//  Created by Gustavo Di Martino on 1/30/13.
//  Copyright (c) 2013 StudioPMG. All rights reserved.
//

#import "SafeNotification.h"

@interface SafeNotification ()
{
    NSMutableDictionary* _userInfo;
    NSString* _name;
    id _object;
    BOOL _performInlineIfPossible;
}

@end

@implementation SafeNotification

@synthesize name = _name;
@synthesize object = _object;
@synthesize performInlineIfPossible = _performInlineIfPossible;

+ (SafeNotification*) notificationWithName: (NSString*) name
{
    return [[SafeNotification alloc] initWithName: name];
}

+ (SafeNotification*) notificationWithName: (NSString*) name object: (id) object
{
    return [[SafeNotification alloc] initWithName: name object: object];
}

- (id) initWithName: (NSString*) name
{
    return [self initWithName: name object: nil];
}

- (id) initWithName: (NSString*) name object: (id) object
{
    self = [super init];
    if ( self )
    {
        _name = name;
        _object = object;
        _userInfo = [NSMutableDictionary dictionary];
        _performInlineIfPossible = YES;
    }
    
    return self;
}

- (id) objectForKeyedSubscript: (id) key
{
    return [_userInfo objectForKeyedSubscript: key];
}

- (void) setObject: (id) object forKeyedSubscript: (id < NSCopying >) key
{
    [_userInfo setObject: object forKeyedSubscript: key];
}

- (void) setObject: (id) object forKey: (id < NSCopying >) key
{
    [_userInfo setObject: object forKey: key];
}

- (void) setValue: (id) value forKey: (NSString*) key
{
    [_userInfo setValue: value forKey: key];
}

- (void) removeObjectForKey: (id) key
{
    [_userInfo removeObjectForKey: key];
}

- (void) removeAllObjects
{
    [_userInfo removeAllObjects];
}

- (void) notify
{
    if ( [NSThread isMainThread] == NO || _performInlineIfPossible == NO )
    {
        [self performSelectorOnMainThread: @selector(doNotify)
                               withObject: nil
                            waitUntilDone: NO];
        return;
    }

    [self doNotify];
}

- (void) doNotify
{
    [[NSNotificationCenter defaultCenter] postNotificationName: _name
                                                        object: _object
                                                      userInfo: _userInfo.count ? _userInfo : nil];
}

@end
